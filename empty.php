<!doctype html>

<html lang="en">

<head>
    <meta charset="utf-8">

    <title>Mimram</title>
    <meta name="description" content="Mimram">
    <meta name="author" content="SitePoint">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick.css" />
    <link rel="stylesheet" href="dist/css/main.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/fd007505c4.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
        integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
        crossorigin="anonymous"></script>



</head>

<body>
    <header class="header">
        <div class="header__wrapper">
            <div class="header__logo logo">
                <a href="index.php"><img src="assets/img/logo-trans.png" /></a>
            </div>
            <div class="header__nav nav">
                <ul>
                    <li>
                        <a href="aboutus.php">about us</a>
                        <ul>
                            <li>
                                <a href="aboutus.php">About Us</a>
                            </li>
                            <li>
                                <a href="profiles.php">Profiles</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href=" index.php#ourCompanies__section">our companies</a>
                        <ul>
                            <li>
                                <a href="ponspecial.php">PON Special Works</a>
                            </li>
                            <li>
                                <a href="mimramengineering.php">Mimram Engineering Service</a>
                            </li>
                            <li>
                                <a href="mimramspecial.php">Mimram Special Works</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="casestudies.php">case studies</a>
                        <ul>
                            <li>
                                <a href="higgins.php">Gabriel Square, St Albans</a>
                            </li>
                            <li>
                                <a href="engie.php">Purley Centre Development, Marsh Farm</a>
                            </li>
                            <li>
                                <a href="lovegrove.php">No1 Lounges, Luton</a>
                            </li>
                            <li>
                                <a href="fattorie.php">Fattorie Garofalo, London Luton Airport</a>
                            </li>
                            <li>
                                <a href="county.php">Fairland's Primary School, Hertfordshire</a>
                            </li>
                            <li>
                                <a href="morgan.php">Oaklands College, Welwyn Garden City</a>
                            </li>
                            <li>
                                <a href="imtech.php">John Radcliffe Hospital - Healthcare Sector</a>
                            </li>
                            <li>
                                <a href="luton.php">London Luton Airport, Luton</a>
                            </li>
                            <li>
                                <a href="ibc.php">IBC Vehicles, Luton</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="contact.php">contact</a></li>
                </ul>
            </div>
            <div class="header__contact contact">
                <ul>
                    <li>
                        <a href="mailto:info@mimramservices.com"><i class="fas fa-envelope"></i> info@mimramservices.com</a>
                    </li>
                    <li>
                        <a href="tel:01582 568212"><i class="fas fa-phone-alt"></i> 01582 568212</a>
                    </li>
                </ul>
            </div>
            <div class="header__hamburger">
                <div id="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
    </header>


    <div class="banner">
        <div class="banner__main">
            <div class="banner__slider">
                <div class="slider__content">
                    <div class="slider__items">
                        <div><img src="assets/img/background1.jpg"></div>
                        <div><img src="assets/img/background2.jpg"></div>
                        <div><img src="assets/img/background3.jpg"></div>
                    </div>
                </div>
            </div>
            <div class="banner__card">
                <div class="banner__card__header">
                    <h3>Success built around our people, the environment and building a better place.</h3>
                </div>

                <div class="banner__card__subheader">
                    <h4>We like to do things differently and in a way that enhances a modern world.</h4>
                </div>
                <div class="banner__card__text">
                    <h5>
                        Established in 1985, we are a, specialist design and build, Mechanical & Electrical contractor.
                        We’ve built our company around <span class="orange">our people</span>, encouraging the
                        development of solutions that <span class="orange">help the environment rather than destroying
                            it.</span> We adhere to a <span class="orange">strong company ethos</span>, in that we care
                        for clients, the environment and the people that work with us.
                    </h5>
                </div>xz
                <div class="banner__card__link">
                    <a href="#">view our <span class="bold">companies >></span></a>
                </div>
            </div>
        </div>
        <div class="banner__empty">

        </div>
    </div>


    <footer>
        <div class="footer__section1">
            <div class="footer__container">
                <div class="footer__section1__header">
                    <h5>Contact Us</h5>
                </div>
                <div class="footer__section1__links">
                    <ul class="footer">
                        <li class="footer"><a href="aboutus.php">About Us</a></li>
                        <li class="footer"><a href=" index.php#ourCompanies__section">our companies</a></li>
                        <li class="footer"><a href="casestudies.php">case studies</a></li>
                        <li class="footer"><a href="contact.php">contact</a></li>
                    </ul>
                </div>
                <div class="footer__section1__contact">
                    <div class="footer__section1__contact__link1">
                        <div class="footer__section1__contact__link1__logo">
                            <span><i class="fas fa-envelope"></i></span>
                        </div>
                        <div class="footer__section1__contact__link1__text">
                            <a href="mailto:info@mimramservices.com"><p>info@mimramservices.com</p></a>
                        </div>
                    </div>
                    <div class="footer__section1__contact__link2">
                        <div class="footer__section1__contact__link2__logo">
                            <span><i class="fas fa-phone-alt"></i></span>
                        </div>
                        <div class="footer__section1__contact__link2__text">
                            <a href="tel:01582 568212"><p>01582 568212</p></a>
                        </div>
                    </div>
                    <div class="footer__section1__contact__link3">
                        <div class="footer__section1__contact__link3__logo">
                            <span><i class="fas fa-map-marker-alt"></i></span>
                        </div>
                        <div class="footer__section1__contact__link3__text">
                            <p>Unit 27 Sundon Industrial Estate, Dencora Way, Bedfordshire, LU3 3HP</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="footer__section2">
            <div class="footer__section2__logo">
                <div class="footer__section2__logo__image">
                    <div class="footer__section2__logo__image__logo">
                        <img src="assets/img/logo.png">
                    </div>
                </div>
                <div class="footer__section2__logo__text">
                    <p>Efficient, effective engineering solutions</p>
                </div>
            </div>
            <div class="footer__section2__links">

            </div>
        </div>
    </footer>
    <script type="text/javascript" src="assets/js/slick/slick.min.js"></script>
    <script src="assets/js/main.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js"></script>

</body>

</html>