"use strict";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

$(document).ready(function () {
  $(document).scroll(function () {
    $('.header').toggleClass('navScrolled', $(this).scrollTop() > 1);
    $('.header li ul li').toggleClass('navScrolled', $(this).scrollTop() > 1);
  });
  $('.quotes').slick({
    dots: true,
    arrows: false,
    prevArrow: ".arrow_prev__banner",
    nextArrow: ".arrow_next__banner",
    fade: true,
    speed: 900,
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
    responsive: [{
      breakpoint: 1950,
      settings: {
        arrows: false
      }
    }]
  });
  $('.sectorSlider').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: ".arrow_prev__banner_sector",
    nextArrow: ".arrow_next__banner_sector",
    autoplay: false,
    responsive: [{
      breakpoint: 1230,
      settings: 'unslick'
    }]
  });
  $(window).resize(function () {
    var windowWidth = $(window).width();

    if (windowWidth > 1230) {
      $('.sectorSlider').slick('refresh');
    }
  });
  $('.accreditations_slider .accreditation_row').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    autoplay: false,
    prevArrow: '.arrow_prev',
    nextArrow: '.arrow_next',
    responsive: [{
      breakpoint: 975,
      settings: _defineProperty({
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1
      }, "arrows", false)
    }]
  });
  $("#hamburger").click(function () {
    $(this).toggleClass("open");
    $(".nav").toggleClass("nav--active");
    $("body").toggleClass("noFlow");
  }); // Validation

  $('form.form').validate({
    ignore: ":hidden",
    rules: {},
    submitHandler: function submitHandler(form) {
      $.ajax({
        type: "POST",
        url: "process.php",
        data: $(form).serialize(),
        success: function success() {
          $('.contentTile__form > .in > h3').text('Your message has now been sent.');
          $('<p>Someone will be in contact with you as soon as possible regarding your enquiry.</p>').insertBefore('.contentTile__form > .in form');
          $('.contentTile__form > .in form').addClass('nonVisible');
        }
      });
      return false; // required to block normal submit since you used ajax
    }
  });
  $(".banner__main .slider__items").slick({
    dots: true,
    arrows: true,
    prevArrow: ".arrow_prev__banner",
    nextArrow: ".arrow_next__banner",
    fade: true,
    speed: 900,
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
    responsive: [{
      breakpoint: 1950,
      settings: {
        arrows: false
      }
    }]
  });
});

function scrollCompany() {
  $([document.documentElement, document.body]).animate({
    scrollTop: $(".ourCompanies").offset().top
  }, 500);
}

;
$("#showMechanical").click(function () {
  $("#moreMechanical").toggle();

  if ($("#moreMechanical").css('display') == 'block') {
    $("#showMechanical").text("Close");
  } else {
    $("#showMechanical").text("Read More >>>");
  }
});
$("#showElectrical").click(function () {
  $("#moreElectrical").toggle();

  if ($("#moreElectrical").css('display') == 'block') {
    $("#showElectrical").text("Close");
  } else {
    $("#showElectrical").text("Read More >>>");
  }
});