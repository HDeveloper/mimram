<!doctype html>

<html lang="en">

<head>
    <meta charset="utf-8">

    <title>Fattorie Garofalo - Mimram</title>

    <meta name="author" content="SitePoint">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick.css" />
    <link rel="stylesheet" href="dist/css/main.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/fd007505c4.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>



</head>

<body>
   <header class="header">
        <div class="header__wrapper">
            <div class="header__logo logo">
                <a href="index.php"><img src="assets/img/logo-trans.png" /></a>
            </div>
            <div class="header__nav nav">
                <ul>
                    

                    <li>
                        <a href="index.php#ourCompanies__section">our family of companies</a>
                        <ul>
                            <li>
                                <a href="ponspecial.php">PON Special Works</a>
                            </li>
                            <li>
                                <a href="mimramengineering.php">Mimram Engineering Service</a>
                            </li>
                            <li>
                                <a href="mimramspecial.php">Mimram Special Works</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="whatwedo.php">what we do</a>
                        <ul>
                            <li>
                                <a href="whatwedo.php#our_services">Our Services</a>
                            </li>
                            <li>
                                <a href="whatwedo.php#our_commitments">Our Commitments</a>
                            </li>
                            <li>
                                <a href="whatwedo.php#our_quality">Quality</a>
                            </li>
                            <li>
                                <a href="whatwedo.php#our_accreditations">Accreditations</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="casestudies.php">case studies</a>
                        
                    </li>
                    <li>
                        <a href="aboutus.php">about us</a>
                        <ul>
                            <li>
                                <a href="aboutus.php">About Us</a>
                            </li>
                            <li>
                                <a href="values.php">Our Values</a>
                            </li>
                            <li>
                                <a href="profiles.php">Profiles</a>
                            </li>
                            <li>
                                <a href="history.php">Our History</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="contact.php">contact</a></li>
                </ul>
            </div>
            <div class="header__contact contact">
                <ul>
                    <li>
                        <a href="mailto:info@mimramservices.com"><i class="fas fa-envelope"></i>
                            info@mimramservices.com</a>
                    </li>
                    <li>
                        <a href="tel:01582 568212"><i class="fas fa-phone-alt"></i> 01582 568212</a>
                    </li>
                </ul>
            </div>
            <div class="header__hamburger">
                <div id="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
    </header>


    <section class="header__images">
        <div class="header__image"> <img src="assets/img/fattorie3.jpg"> </div>
    </section>
    <div class="caseStoryInfo">
        <div class="container">
            <div class="companies__header">
                <h1><span>Fattorie Garofalo, London Luton Airport</span></h1>
            </div>
            <div class="caseStoryInfo__text">
                <div class="caseStoryInfo__text__split">
                    <p>As the first new high end food retail outlet,Fattorie Garofalo unit in the UK at London Luton
                        Airport’s newly refurbished terminal building. Through references and recommendations from
                        London Luton Airport projects team we were approached by Garofalo’s to provide a complete
                        turnkey solution for the new store. This comprised to not only fit the design brief but
                        importantly manage any interfaces of that design with both UK and Aviation sector standards.
                        <br><br></p>
                </div>
                <div class="caseStoryInfo__text__split">
                    <p>
                        The project involved installation of a new temporary store on design and build basis, providing
                        construction, fit out building works and full mechanical and electrical services alongside
                        technical compliance. Key works consisted of Fire and Safety systems linked to the LLA
                        centralised systems to both the temporary store and the full retail outlet.
                    </p>
                </div>


            </div>
        </div>
    </div>

    <div class="caseStudy__info">
        <div class="container">
            <div class="caseStudy__info__column">
               <div class="caseStudy__info__box">
                    <div class="caseStudy__info__box__icon">
                        <span><i class="fas fa-user"></i></span>
                    </div>
                    <div class="caseStudy__info__box__header">
                        <h2>Client</h2>
                    </div>
                    <div class="caseStudy__info__box__text">
                        <h5>Fattorie Garofalo</h5>
                    </div>
                </div>
                <div class="caseStudy__info__box">
                    <div class="caseStudy__info__box__icon">
                        <span><i class="fas fa-pound-sign"></i></span>
                    </div>
                    <div class="caseStudy__info__box__header">
                        <h2>Contract Value</h2>
                    </div>
                    <div class="caseStudy__info__box__text">
                        <h5>£236,745</h5>
                    </div>
                </div>
                <div class="caseStudy__info__box">
                    <div class="caseStudy__info__box__icon">
                        <span><i class="far fa-clock"></i></span>
                    </div>
                    <div class="caseStudy__info__box__header">
                        <h2>Contract Duration</h2>
                    </div>
                    <div class="caseStudy__info__box__text">
                        <h5>3 months</h5>
                    </div>
                </div>
                <div class="caseStudy__info__box">
                    <div class="caseStudy__info__box__icon">
                        <span><i class="fas fa-map-marker-alt"></i></span>
                    </div>
                    <div class="caseStudy__info__box__header">
                        <h2>Location</h2>
                    </div>
                    <div class="caseStudy__info__box__text">
                        <h5>London Luton Airport (Departures), Luton, Bedfordshire</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="footer__section1">
            <div class="footer__container">
                <div class="footer__section1__header">
                    <h5>Contact Us</h5>
                </div>
                <div class="footer__section1__links">
                    <ul class="footer">
                        <li class="footer"><a href="index.php#ourCompanies__section">Our Family of Companies</a></li>
                        <li class="footer"><a href="whatwedo.php">What We Do</a></li>
                        <li class="footer"><a href="casestudies.php">case studies</a></li>
                        <li class="footer"><a href="aboutus.php">About Us</a></li>
                        <li class="footer"><a href="contact.php">contact</a></li>
                    </ul>
                </div>
                <div class="footer__section1__contact">
                    <div class="footer__section1__contact__link1">
                        <div class="footer__section1__contact__link1__logo">
                            <span><i class="fas fa-envelope"></i></span>
                        </div>
                        <div class="footer__section1__contact__link1__text">
                            <a href="mailto:info@mimramservices.com">
                                <p>info@mimramservices.com</p>
                            </a>
                        </div>
                    </div>
                    <div class="footer__section1__contact__link2">
                        <div class="footer__section1__contact__link2__logo">
                            <span><i class="fas fa-phone-alt"></i></span>
                        </div>
                        <div class="footer__section1__contact__link2__text">
                            <a href="tel:01582 568212">
                                <p>01582 568212</p>
                            </a>
                        </div>
                    </div>
                    <div class="footer__section1__contact__link3">
                        <div class="footer__section1__contact__link3__logo">
                            <span><i class="fas fa-map-marker-alt"></i></span>
                        </div>
                        <div class="footer__section1__contact__link3__text">
                            <p>Unit 27 Sundon Industrial Estate, Dencora Way, Bedfordshire, LU3 3HP</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="footer__section2">
            <div class="footer__button">
                <a href="contact.php">
                    <button class="contact"><span>Contact Mimram</span></button>
                </a>
            </div>
            <div class="footer__section2__logo">
                <div class="footer__section2__logo__image">
                    <div class="footer__section2__logo__image__logo">
                        <img src="assets/img/logo.png">
                    </div>
                </div>
                <div class="footer__section2__logo__text">
                    <p>Efficient, effective engineering solutions</p>
                </div>
            </div>
            <div class="footer__section2__links">
                <div class="footer__section2__links">
                    <ul class="footer">
                        <li class="footer footerRight"><a class="footerRight">COPYRIGHT 2020</a></li>
                        <li class="footer"><a href="privacy.php" class="footerRight">PRIVACY</a></li>
                        <li class="footer"><a href="terms.php" class="footerRight">TERMS</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <script type="text/javascript" src="assets/js/slick/slick.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js"></script>

</body>

</html>
