<!doctype html>

<html lang="en">

<head>
    <meta charset="utf-8">

    <title>Mimram</title>
    <meta name="description" content="Mimram">
    <meta name="author" content="SitePoint">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick.css" />
    <link rel="stylesheet" href="dist/css/main.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/fd007505c4.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
        integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
        crossorigin="anonymous"></script>



</head>

<body>
   <header class="header">
        <div class="header__wrapper">
            <div class="header__logo logo">
                <a href="index.php"><img src="assets/img/logo-trans.png" /></a>
            </div>
            <div class="header__nav nav">
                <ul>
                    

                    <li>
                        <a href="index.php#ourCompanies__section">our family of companies</a>
                        <ul>
                            <li>
                                <a href="ponspecial.php">PON Special Works</a>
                            </li>
                            <li>
                                <a href="mimramengineering.php">Mimram Engineering Service</a>
                            </li>
                            <li>
                                <a href="mimramspecial.php">Mimram Special Works</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="whatwedo.php">what we do</a>
                        <ul>
                            <li>
                                <a href="whatwedo.php#our_services">Our Services</a>
                            </li>
                            <li>
                                <a href="whatwedo.php#our_commitments">Our Commitments</a>
                            </li>
                            <li>
                                <a href="whatwedo.php#our_quality">Quality</a>
                            </li>
                            <li>
                                <a href="whatwedo.php#our_accreditations">Accreditations</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="casestudies.php">case studies</a>
                        
                    </li>
                    <li>
                        <a href="aboutus.php">about us</a>
                        <ul>
                            <li>
                                <a href="aboutus.php">About Us</a>
                            </li>
                            <li>
                                <a href="values.php">Our Values</a>
                            </li>
                            <li>
                                <a href="profiles.php">Profiles</a>
                            </li>
                            <li>
                                <a href="history.php">Our History</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="contact.php">contact</a></li>
                </ul>
            </div>
            <div class="header__contact contact">
                <ul>
                    <li>
                        <a href="mailto:info@mimramservices.com"><i class="fas fa-envelope"></i>
                            info@mimramservices.com</a>
                    </li>
                    <li>
                        <a href="tel:01582 568212"><i class="fas fa-phone-alt"></i> 01582 568212</a>
                    </li>
                </ul>
            </div>
            <div class="header__hamburger">
                <div id="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
    </header>

    <div class="banner">
        <div class="banner__main">
            <div class="banner__slider">
                <div class="slider__content">
                    <div class="slider__items">
                        <div><img src="assets/img/background1.jpg"></div>
                        <div><img src="assets/img/background2.jpg"></div>
                        <div><img src="assets/img/background3.jpg"></div>
                    </div>
                </div>
            </div>
            <div class="banner__card">
                <div class="banner__card__header">
                    <h3>Success built around our people, the environment and building a better place.</h3>
                </div>

                <div class="banner__card__subheader">
                    <h4>We like to do things differently and in a way that enhances a modern world.</h4>
                </div>
                <div class="banner__card__text">
                    <h5>
                        Established in 1985, we are a, specialist design and build, Mechanical & Electrical contractor.
                        We’ve built our company around <span class="orange">our people</span>, encouraging the
                        development of solutions that <span class="orange">help the environment rather than destroying
                            it.</span> We adhere to a <span class="orange">strong company ethos</span>, in that we care
                        for clients, the environment and the people that work with us.
                    </h5>
                </div>xz
                <div class="banner__card__link">
                    <a href="#">view our <span class="bold">companies >></span></a>
                </div>
            </div>
        </div>
        <div class="banner__empty">

        </div>
    </div>


    <section class="type1">
        <div class="section__type1">
            <div class="section__type1__main">
                <div class="section__type1__left">
                    <div class="container">
                        <div class="logo">
                            <a href="index.php"><img src="assets/img/logolq.png"></a>
                        </div>
                        <div class="text">
                            <h1>Over 40</h1>
                            <h3>years experience</h3>
                        </div>
                    </div>
                </div>
                <div class="section__type1__right">
                    <div class="container">

                        <h4>Committed to innovation we take pride in our delivery of a full range of Mechanical &
                            Electrical services that are on time, in spec and within budget.</h4>
                        <p>
                            What sets us apart from others is our people. We believe in a
                            trust based culture and core values, allowing our team to not
                            only manage projects but also manage relationships with our
                            clients that are long term and ones that are built on
                            expectation and reliability.
</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="inform">
        <div class="inform__leftSide">
            <div class="inform__leftSide__picture">
                <img src="assets/img/section1mainpic.PNG">
            </div>
            <div class="inform__leftSide__section1 skyBlue">
                <div class="inform__leftSide__section1__content">
                    <div class="container">
                        <h2>Mechanical</h2>
                        <p>For residential, commercial or retail projects, our dedicated and multidiscipline team of
                            plumbers, fitters, gas safe engineers and refcom engineers have a vast depth in knowledge
                            and skill. They provide a full range of plumbing, drainage and heating solutions, for all
                            sizes of mechanical installations. As an approved Contractor with leading manufacturers our
                            team have built the reputation on high quality and standards. This varies from the basics of
                            gas, water and drainage, through to solutions that comply with regulatory challenges, our
                            team offer solutions that are price competitive and cost effective.</p>
                        <ul class="circle">
                            <li class="circle"><a>Pipework</a></li>
                            <li class="circle"><a>Design install</a></li>
                            <li class="circle"><a>Domestic and Commercial gas installation</a></li>
                            <li class="circle"><a>Above ground drainage</a></li>
                            <li class="circle"><a>LTHW Systems</a></li>
                            <li class="circle"><a>Central Plant</a></li>
                            <li class="circle"><a>Rainwater Services</a></li>
                            <li class="circle"><a>Fabrication works</a></li>
                            <li class="circle"><a>Ventilation Systems</a></li>
                            <li class="circle"><a>Air conditioning</a></li>
                            <li class="circle"><a>Underfloor Heating Systems</a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="inform__rightSide">
            <div class="inform__rightSide__section1 darkBlue">
                <div class="inform__rightSide__section1__content">
                    <div class="container">
                        <h2>Services</h2>
                        <p>Our strength as a business lies within our wealth of experience and skills held by our
                            people. They are highly experienced staff who maintain independent and specialist knowledge
                            to the services we offer. </p>
                    </div>
                </div>
                <div class="inform__rightSide__section1__border"></div>
            </div>
            <div class="inform__rightSide__section2">
                <div class="inform__rightSide__section2__content">
                    <div class="container">
                        <h2>Electrical</h2>
                        <p>Electrical installations are business-critical services. From emergency lighting and power
                            systems through to bespoke uninterruptable power distribution systems. We have delivered
                            landmark installations which we pride ourselves on. Not only providing our service but
                            working in collaboration with our clients to align their visions and ensure customer
                            satisfaction with quality and engineering expertise.</p>
                        <ul class="circle">
                            <li class="circle">AV Solutions</li>
                            <li class="circle">Low Voltage</li>
                            <li class="circle">Small Power Installation</li>
                            <li class="circle">Lighting (internal, External & Emergency)</li>
                            <li class="circle">Multi-failsafe UPS </li>
                            <li class="circle">CCTV </li>
                            <li class="circle">Intruder Alarm and Access Systems </li>
                            <li class="circle">Fire detection Systems, Fire Detection </li>
                            <li class="circle">Home Automation</li>
                            <li class="circle">Electrical Testing</li>
                            <li class="circle">PAT Testing</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="inform__rightSide__section3">
                <div class="inform__rightSide__section3__content">
                    <div class="container">
                        <h2>Design & Build</h2>
                        <p>Our dynamic, in-house engineering team has the full range of contemporary design technologies
                            at its fingertips. We not only focus on simplifying project management but help to keep the
                            budget on track too, using craft solutions that are cost effective and energy efficient.
                        </p>
                    </div>
                </div>
                <div class="inform__rightSide__section3__button">
                    <a href="contact.php">
                        <button class="contact"><span>Contact Mimram</span></button>
                    </a>
                </div>
            </div>
            <div class="inform__rightSide__section4">
                <div class="inform__rightSide__section4__content">
                    <div class="container">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="splitImages">
        <div class="splitImages__1">
            <div class="splitImages__card">
                <div class="splitImages__card__quote">
                    <i class="fas fa-quote-left"></i>
                </div>
                <div class="splitImages__card__text">
                    <h5>We care for clients, the environment and the people that with us.</h5>
                </div>
            </div>
        </div>
        <div class="splitImages__2">
        </div>

    </section>


    <section class="ourCompanies">
        <div class="ourCompanies__header">
            <h4>Our <span class="navyBlue">COMPANIES</span></h1>
        </div>
        <div id="ourCompanies__slider">

            <div class="container">
                <div class="slick__content">

                    <div class="slick__items">

                        <div class="test"><img src="assets/img/200.png"></div>
                        <div class="test"><img src="assets/img/200.png"></div>
                        <div class="test"><img src="assets/img/200.png"></div>
                        <div class="test"><img src="assets/img/200.png"></div>
                        <div class="test"><img src="assets/img/200.png"></div>
                        <div class="test"><img src="assets/img/200.png"></div>

                    </div>
                    <div class="arrow_prev arrow">
                        <span><i class="fas fa-chevron-left"></i></span>
                    </div>
                    <div class="arrow_next arrow">
                        <span><i class="fas fa-chevron-right"></i></span>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="sectors">
        <div class="container">
            <div class="sectors__header">
                <h4 class="navyBlue">Sectors</h4>
            </div>
            <div class="sectors__left">
                <div class="sectors__infobox">
                    <div class="sectors__infobox__left">
                        <div class="sectors__infobox__box">
                            <h5>Commercial</h5>
                            <p>Offering efficient and practical solutions to projects of all shapes and sizes; from new
                                buildings, large and refurbished office projects to high end retail outlets.</p>
                        </div>
                        <div class="sectors__infobox__box">
                            <h5>Residential </h5>
                            <p>From basic refits, to a high end multimillion pound flat complexes, our teams partner up
                                with clients from start to finish ensuring the success of every project.</p>
                        </div>
                        <div class="sectors__infobox__box">
                            <h5>Industrial</h5>
                            <p>Our team have experience in completing an immense variety of industrial projects such as
                                engineering plants, rubber moulding plants, paint shops, etc. They understand the
                                requirements to ensure utilisation and all challenges are met.</p>
                        </div>
                        <div class="sectors__infobox__box">
                            <h5>Education </h5>
                            <p>Acknowledging the need to inspire the future of tomorrow; our team collaborate with
                                clients to ensure a secure, trusted and comfortable environment is provided to children
                                and adults. Where knowledge can be developed and shared</p>
                        </div>
                    </div>
                    <div class="sectors__infobox__right">
                        <div class="sectors__infobox__box">
                            <h5>Healthcare </h5>
                            <p>Helping to provide tailored healthcare solutions to the sector. Whether Pharmaceuticals,
                                Care/ Residential homes or Hospitals, we understand the necessity for each project, to
                                fit the needs of patient requirements, NHS professionals and the surrounding
                                communities.</p>
                        </div>
                        <div class="sectors__infobox__box">
                            <h5>Sports & Leisure </h5>
                            <p>As one of the largest and fastest growing sectors, we understand the need of project
                                completion within a cost efficient and short time frame. To keep up with the constant
                                progression as new activities and equipment are developed, we help clients to fulfil
                                requirements without hindering individual and community needs for achieving personal and
                                health goals.</p>
                        </div>
                        <div class="sectors__infobox__box">
                            <h5>Aviation </h5>
                            <p>From Fuel Farms to Airside/Landside our team have over 20 years’ experience working
                                within the aviation industry ensuring a seamless delivery of expectations. We are
                                familiar with the sensitivity that is required within the aviation environment.</p>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <div class="sectors__right">
            <div class="sectors__img1">
                <img src="assets/img/sector1.jpg">
            </div>
            <div class="sectors__img2">
                <img src="assets/img/sector2.jpg">
            </div>
        </div>
    </section>
    <section class="accreditations">
        <div class="container">
            <div class="accreditations__content">


                <div class="accreditations__image">
                    <div class="accreditations__image__image">
                        <img src="assets/img/background1.jpg">
                    </div>
                </div>
                <div class="accreditations__text">
                    <div class="accreditations__header">
                        <h5>Accreditations</h5>
                    </div>
                    <div class="accreditations__info">
                        <p>An accredited member of the Safe Contractor scheme, Mimram’s health & safety policy is
                            supported by stringent guidelines, ongoing training and regular on-site inspections, a major
                            contribution to our excellent health and safety record. Our full time, in-house health and
                            safety managers work closely with external Health and Safety consultants to ensure
                            outstanding safety practices.<br><br>
                            Quality control is critical to successful M&E contracting, and we maintain an in-house,
                            full-time quality control department. Our quality management system, which incorporates
                            strategies for continual improvement, has been QMS accredited to the global benchmark
                            standard ISO 9001.<br><br>
                            Naturally, appropriate certifications ensure that our technicians, electricians and
                            mechanical engineers deliver outstanding quality whilst remaining highly price-competitive.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="accreditations__card__1">
                <div class="accreditations__card__1__image">
                    <img src="assets/img/iso.png">
                </div>
            </div>

            <div class="accreditations__card__2">
                <div class="accreditations__card__quote">
                    <i class="fas fa-quote-left"></i>
                </div>
                <div class="accreditations__card__text">
                    <h5>Success built around our people, the environment and building a better place</h5>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="footer__section1">
            <div class="footer__container">
                <div class="footer__section1__header">
                    <h5>Contact Us</h5>
                </div>
                <div class="footer__section1__links">
                    <ul class="footer">
                        <li class="footer"><a href="index.php#ourCompanies__section">Our Family of Companies</a></li>
                        <li class="footer"><a href="whatwedo.php">What We Do</a></li>
                        <li class="footer"><a href="casestudies.php">case studies</a></li>
                        <li class="footer"><a href="aboutus.php">About Us</a></li>
                        <li class="footer"><a href="contact.php">contact</a></li>
                    </ul>
                </div>
                <div class="footer__section1__contact">
                    <div class="footer__section1__contact__link1">
                        <div class="footer__section1__contact__link1__logo">
                            <span><i class="fas fa-envelope"></i></span>
                        </div>
                        <div class="footer__section1__contact__link1__text">
                            <a href="mailto:info@mimramservices.com"><p>info@mimramservices.com</p></a>
                        </div>
                    </div>
                    <div class="footer__section1__contact__link2">
                        <div class="footer__section1__contact__link2__logo">
                            <span><i class="fas fa-phone-alt"></i></span>
                        </div>
                        <div class="footer__section1__contact__link2__text">
                            <a href="tel:01582 568212"><p>01582 568212</p></a>
                        </div>
                    </div>
                    <div class="footer__section1__contact__link3">
                        <div class="footer__section1__contact__link3__logo">
                            <span><i class="fas fa-map-marker-alt"></i></span>
                        </div>
                        <div class="footer__section1__contact__link3__text">
                            <p>Unit 27 Sundon Industrial Estate, Dencora Way, Bedfordshire, LU3 3HP</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="footer__section2">
            <div class="footer__section2__logo">
                <div class="footer__section2__logo__image">
                    <div class="footer__section2__logo__image__logo">
                        <img src="assets/img/logo.png">
                    </div>
                </div>
                <div class="footer__section2__logo__text">
                    <p>Efficient, effective engineering solutions</p>
                </div>
            </div>
            <div class="footer__section2__links">

            </div>
        </div>
    </footer>
    <script type="text/javascript" src="assets/js/slick/slick.min.js"></script>
    <script src="assets/js/main.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js"></script>

</body>

</html>