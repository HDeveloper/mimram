<!doctype html>

<html lang="en">

<head>
    <meta charset="utf-8">

    <title>Case Studies - Mimram</title>
    <meta name="description" content="Mimram">
    <meta name="author" content="SitePoint">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick.css" />
    <link rel="stylesheet" href="dist/css/main.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/fd007505c4.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>



</head>

<body>
    <header class="header">
        <div class="header__wrapper">
            <div class="header__logo logo">
                <a href="index.php"><img src="assets/img/logo-trans.png" /></a>
            </div>
            <div class="header__nav nav">
                <ul>
                    <li>
                        <a href="aboutus.php">about us</a>
                        <ul>
                            <li>
                                <a href="aboutus.php">About Us</a>
                            </li>
                            <li>
                                <a href="profiles.php">Profiles</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href=" index.php#ourCompanies__section">our companies</a>
                        <ul>
                            <li>
                                <a href="ponspecial.php">PON Special Works</a>
                            </li>
                            <li>
                                <a href="mimramengineering.php">Mimram Engineering Service</a>
                            </li>
                            <li>
                                <a href="mimramspecial.php">Mimram Special Works</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="casestudies.php">case studies</a>
                        <ul>
                            <li>
                                <a href="higgins.php">Gabriel Square, St Albans</a>
                            </li>
                            <li>
                                <a href="engie.php">Purley Centre Development, Marsh Farm</a>
                            </li>
                            <li>
                                <a href="lovegrove.php">No1 Lounges, Luton</a>
                            </li>
                            <li>
                                <a href="fattorie.php">Fattorie Garofalo, London Luton Airport</a>
                            </li>
                            <li>
                                <a href="county.php">Fairland's Primary School, Hertfordshire</a>
                            </li>
                            <li>
                                <a href="morgan.php">Oaklands College, Welwyn Garden City</a>
                            </li>
                            <li>
                                <a href="imtech.php">John Radcliffe Hospital - Healthcare Sector</a>
                            </li>
                            <li>
                                <a href="luton.php">London Luton Airport, Luton</a>
                            </li>
                            <li>
                                <a href="ibc.php">IBC Vehicles, Luton</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="contact.php">contact</a></li>
                </ul>
            </div>
            <div class="header__contact contact">
                <ul>
                    <li>
                        <a href="mailto:info@mimramservices.com"><i class="fas fa-envelope"></i> info@mimramservices.com</a>
                    </li>
                    <li>
                        <a href="tel:01582 568212"><i class="fas fa-phone-alt"></i> 01582 568212</a>
                    </li>
                </ul>
            </div>
            <div class="header__hamburger">
                <div id="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
    </header>

    <section class="cases__banner">
        <div class="container">
            <div class="cases__flex">
                <div class="cases__header">
                    <h2 class="navyBlue">Case Studies</h2>
                </div>
                <div class="cases__subheader">
                    <h4>Amongst our extensive client base we have completed various projects over the years.</h4>
                </div>
                <div class="cases__text">
                    <p>Often offering whole package solutions and logistics we are used to adapting to tight time frames and cost restraints. We frequently come up against a raft of challenges whereby we are ensuring a design brief is fulfilled and various sector standards are adheredto and met. Full installations often require not just construction but also full mechanical and electrical services alongside technical compliance.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="cases">
        <div class="caseStudy">
            <div class="caseStudy__image">
                <img src="assets/img/gabriel.jpg">
            </div>
            <div class="caseStudy__text">
                <div class="caseStudy__text__container">
                    <div class="caseStudy__text__header">
                        <h2 class="navyBlue">Higgins Construction</h2>
                    </div>
                    <div class="caseStudy__text__text">
                        <p>Appointed as a design and build contractor, to carry out a vast variety of full Mechanical &Electrical Services to this new complex consisting of 80 apartments in St Albans, Hertfordshire.</p>
                    </div>
                    <div class="caseStudy__text__button">
                        <a href="higgins.php"><button class="cases" href="">READ MORE</button></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="caseStudy">
            <div class="caseStudy__text">
                <div class="caseStudy__text__container">
                    <div class="caseStudy__text__header">
                        <h2 class="navyBlue">Engie Construction</h2>
                    </div>
                    <div class="caseStudy__text__text">
                        <p>Project included full mechanical works to a brand new 94 plot of housing for Luton borough council development initiative and comprised of 3 blocks totalling 83 flats and 11 houses. </p>
                    </div>
                    <div class="caseStudy__text__button">
                        <div class="caseStudy__text__button">
                            <a href="engie.php"><button class="cases" href="">READ MORE</button></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="caseStudy__image">
                <img src="assets/img/marsh.jpg">
            </div>

        </div>

        <div class="caseStudy">
            <div class="caseStudy__image">
                <img src="assets/img/case_no1__1.JPG">
            </div>
            <div class="caseStudy__text">
                <div class="caseStudy__text__container">
                    <div class="caseStudy__text__header">
                        <h2 class="navyBlue">AA Lovegrove</h2>
                    </div>
                    <div class="caseStudy__text__text">
                        <p>Work included delivery on a design and build basis and complete mechanical and electrical solution to the Clients (WSP) stage 3 design of the Executive Lounge. Our specialist services included food preparation kitchens and three separate lounge spaces.</p>
                    </div>
                    <div class="caseStudy__text__button">
                        <a href="lovegrove.php"><button class="cases" href="">READ MORE</button></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="caseStudy">
            <div class="caseStudy__text">
                <div class="caseStudy__text__container">
                    <div class="caseStudy__text__header">
                        <h2 class="navyBlue">Fattorie Garofalo</h2>
                    </div>
                    <div class="caseStudy__text__text">
                        <p>As the first new high end food retail outlet, Fattorie Garofalo unit in the UK at London Luton Airport’s newly refurbished terminal building. Through references and recommendations from London Luton Airport projects team we were approached by Garofalo’s to provide a complete turnkey solution for the new store. </p>
                    </div>
                    <div class="caseStudy__text__button">
                        <div class="caseStudy__text__button">
                            <a href="fattorie.php"><button class="cases" href="">READ MORE</button></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="caseStudy__image">
                <img src="assets/img/fattorie3.jpg">
            </div>

        </div>

        <div class="caseStudy">
            <div class="caseStudy__image">
                <img src="assets/img/image--16--big.jpg">
            </div>
            <div class="caseStudy__text">
                <div class="caseStudy__text__container">
                    <div class="caseStudy__text__header">
                        <h2 class="navyBlue">Hertfordshire County Council</h2>
                    </div>
                    <div class="caseStudy__text__text">
                        <p>Contracted to design, supply and install a variety of electrical and mechanical systems to two new blocks comprising classrooms, group areas, toilet block and standalone plant room, at this large, three form entry primary school.</p>
                    </div>
                    <div class="caseStudy__text__button">
                        <a href="county.php"><button class="cases" href="">READ MORE</button></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="caseStudy">
            <div class="caseStudy__text">
                <div class="caseStudy__text__container">
                    <div class="caseStudy__text__header">
                        <h2 class="navyBlue">Morgan Sindall</h2>
                    </div>
                    <div class="caseStudy__text__text">
                        <p>Works to the Welwyn Garden City campus at this FE College included the design and installation of multiple Mechanical and electrical services to both the Learning Resource and Engineering Resource areas of the college. </p>
                    </div>
                    <div class="caseStudy__text__button">
                        <div class="caseStudy__text__button">
                            <a href="morgan.php"><button class="cases" href="">READ MORE</button></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="caseStudy__image">
                <img src="assets/img/image--19--big.jpg">
            </div>

        </div>

        <div class="caseStudy">
            <div class="caseStudy__image">
                <img src="assets/img/image--25--big.jpg">
            </div>
            <div class="caseStudy__text">
                <div class="caseStudy__text__container">
                    <div class="caseStudy__text__header">
                        <h2 class="navyBlue">Imtech Meica</h2>
                    </div>
                    <div class="caseStudy__text__text">
                        <p>Based on 2 separate extensions consisting of first phase extension to the office areas built above the main entrance and included a PV system installation to the roof to generate renewable electricity.</p>
                    </div>
                    <div class="caseStudy__text__button">
                        <a href="imtech.php"><button class="cases" href="">READ MORE</button></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="caseStudy">
            <div class="caseStudy__text">
                <div class="caseStudy__text__container">
                    <div class="caseStudy__text__header">
                        <h2 class="navyBlue">London Luton Airport</h2>
                    </div>
                    <div class="caseStudy__text__text">
                        <p>As one of the leading preferred contractors working both land and airside at this busy international airport for over 20 years, fulfilling numerous contracts with the requirement of mechanical and electrical services. Scopes of work involved predominately special works and involvement in the major expansion of the airport, including an array of works from the electrical power upgrades including sub stations, power distribution boards to lighting upgrades throughout the entire airport. </p>
                    </div>
                    <div class="caseStudy__text__button">
                        <div class="caseStudy__text__button">
                            <a href="luton.php"><button class="cases" href="">READ MORE</button></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="caseStudy__image">
                <img src="assets/img/image--20--big.jpg">
            </div>

        </div>

        <div class="caseStudy">
            <div class="caseStudy__image">
                <img src="assets/img/image--24--big.jpg">
            </div>
            <div class="caseStudy__text">
                <div class="caseStudy__text__container">
                    <div class="caseStudy__text__header">
                        <h2 class="navyBlue">IBC Vehicles</h2>
                    </div>
                    <div class="caseStudy__text__text">
                        <p>With a long association with IBC we have completed various projects over the years. Our services provided includedmechanical and electrical services design and installation as well as some turnkey developments where we offer a whole package solution to include fabric works.</p>
                    </div>
                    <div class="caseStudy__text__button">
                        <a href="ibc.php"><button class="cases">READ MORE</button></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="footer__section1">
            <div class="footer__container">
                <div class="footer__section1__header">
                    <h5>Contact Us</h5>
                </div>
                <div class="footer__section1__links">
                    <ul class="footer">
                        <li class="footer"><a href="aboutus.php">About Us</a></li>
                        <li class="footer"><a href=" index.php#ourCompanies__section">our companies</a></li>
                        <li class="footer"><a href="casestudies.php">case studies</a></li>
                        <li class="footer"><a href="contact.php">contact</a></li>
                    </ul>
                </div>
                <div class="footer__section1__contact">
                    <div class="footer__section1__contact__link1">
                        <div class="footer__section1__contact__link1__logo">
                            <span><i class="fas fa-envelope"></i></span>
                        </div>
                        <div class="footer__section1__contact__link1__text">
                            <a href="mailto:info@mimramservices.com">
                                <p>info@mimramservices.com</p>
                            </a>
                        </div>
                    </div>
                    <div class="footer__section1__contact__link2">
                        <div class="footer__section1__contact__link2__logo">
                            <span><i class="fas fa-phone-alt"></i></span>
                        </div>
                        <div class="footer__section1__contact__link2__text">
                            <a href="tel:01582 568212">
                                <p>01582 568212</p>
                            </a>
                        </div>
                    </div>
                    <div class="footer__section1__contact__link3">
                        <div class="footer__section1__contact__link3__logo">
                            <span><i class="fas fa-map-marker-alt"></i></span>
                        </div>
                        <div class="footer__section1__contact__link3__text">
                            <p>Unit 27 Sundon Industrial Estate, Dencora Way, Bedfordshire, LU3 3HP</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="footer__section2">
            <div class="footer__button">
                <a href="contact.php">
                    <button class="contact"><span>Contact Mimram</span></button>
                </a>
            </div>
            <div class="footer__section2__logo">
                <div class="footer__section2__logo__image">
                    <div class="footer__section2__logo__image__logo">
                        <img src="assets/img/logo.png">
                    </div>
                </div>
                <div class="footer__section2__logo__text">
                    <p>Efficient, effective engineering solutions</p>
                </div>
            </div>
            <div class="footer__section2__links">
                <div class="footer__section2__links">
                    <ul class="footer">
                        <li class="footer footerRight"><a class="footerRight">COPYRIGHT 2020</a></li>
                        <li class="footer"><a href="privacy.php" class="footerRight">PRIVACY</a></li>
                        <li class="footer"><a href="terms.php" class="footerRight">TERMS</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <script type="text/javascript" src="assets/js/slick/slick.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js"></script>

</body>

</html>
