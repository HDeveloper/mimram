<!doctype html>

<html lang="en">

<head>
    <meta charset="utf-8">

    <title>Higgins Construction - Mimram</title>

    <meta name="description" content="Mimram">
    <meta name="author" content="SitePoint">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick.css" />
    <link rel="stylesheet" href="dist/css/main.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/fd007505c4.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>



</head>

<body>
    <header class="header">
        <div class="header__wrapper">
            <div class="header__logo logo">
                <a href="index.php"><img src="assets/img/logo-trans.png" /></a>
            </div>
            <div class="header__nav nav">
                <ul>
                    

                    <li>
                        <a href="index.php#ourCompanies__section">our family of companies</a>
                        <ul>
                            <li>
                                <a href="ponspecial.php">PON Special Works</a>
                            </li>
                            <li>
                                <a href="mimramengineering.php">Mimram Engineering Service</a>
                            </li>
                            <li>
                                <a href="mimramspecial.php">Mimram Special Works</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="whatwedo.php">what we do</a>
                        <ul>
                            <li>
                                <a href="whatwedo.php#our_services">Our Services</a>
                            </li>
                            <li>
                                <a href="whatwedo.php#our_commitments">Our Commitments</a>
                            </li>
                            <li>
                                <a href="whatwedo.php#our_quality">Quality</a>
                            </li>
                            <li>
                                <a href="whatwedo.php#our_accreditations">Accreditations</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="casestudies.php">case studies</a>
                        
                    </li>
                    <li>
                        <a href="aboutus.php">about us</a>
                        <ul>
                            <li>
                                <a href="aboutus.php">About Us</a>
                            </li>
                            <li>
                                <a href="values.php">Our Values</a>
                            </li>
                            <li>
                                <a href="profiles.php">Profiles</a>
                            </li>
                            <li>
                                <a href="history.php">Our History</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="contact.php">contact</a></li>
                </ul>
            </div>
            <div class="header__contact contact">
                <ul>
                    <li>
                        <a href="mailto:info@mimramservices.com"><i class="fas fa-envelope"></i>
                            info@mimramservices.com</a>
                    </li>
                    <li>
                        <a href="tel:01582 568212"><i class="fas fa-phone-alt"></i> 01582 568212</a>
                    </li>
                </ul>
            </div>
            <div class="header__hamburger">
                <div id="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
    </header>



    <section class="header__images">
        <div class="header__image"> <img src="assets/img/gabriel.jpg"> </div>
    </section>
    <div class="caseStoryInfo">

        <div class="container">
            <div class="companies__header">
                <h1><span>Gabriel Square, St Albans </span></h1>
            </div>
            <div class="caseStoryInfo__text">
                <div class="caseStoryInfo__text__split">
                    <p>Appointed as a design and build contractor, to carry out a vast variety of full Mechanical
                        & Electrical Services to this new complex consisting of 80 apartments in St Albans,
                        Hertfordshire. <br><br>
                        The works to this £6.3m project included Small Power, internal and external lighting, home
                        automation utilising Control 4 systems to each dwelling, fire alarm systems, CCTV and access
                        control, billing solutions, underfloor heating, central plant room, providing regenerated energy
                        through a CHP, BMS controls, domestic water services, heating and hot water produced through a
                        HIU, mechanical heat recovery ventilation.<br><br></p>
                </div>
                <div class="caseStoryInfo__text__split">
                    <p>
                        This project was developed to the higher end of the spectrum with each residence containing
                        around £20k worth of home automation equipment providing centralised control over lighting,
                        heating video door entry and the facility to upgrade to whole home video and audio
                        systems.<br><br>
                        The residences are constructed surrounding a communal courtyard at podium level with parking
                        spaces beneath. The centralised plant and equipment being housed also at car park level below
                        the podium slab. Al services are distributed from the central plant spaces vis this underground
                        car park level via discreet service rafts.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="caseStudy__info">
        <div class="container">
            <div class="caseStudy__info__column">
               <div class="caseStudy__info__box">
                    <div class="caseStudy__info__box__icon">
                        <span><i class="fas fa-user"></i></span>
                    </div>
                    <div class="caseStudy__info__box__header">
                        <h2>Client</h2>
                    </div>
                    <div class="caseStudy__info__box__text">
                        <h5>Higgins Construction</h5>
                    </div>
                </div>
                <div class="caseStudy__info__box">
                    <div class="caseStudy__info__box__icon">
                        <span><i class="fas fa-pound-sign"></i></span>
                    </div>
                    <div class="caseStudy__info__box__header">
                        <h2>Contract Value</h2>
                    </div>
                    <div class="caseStudy__info__box__text">
                        <h5>£6,300,000</h5>
                    </div>
                </div>
                <div class="caseStudy__info__box">
                    <div class="caseStudy__info__box__icon">
                        <span><i class="far fa-clock"></i></span>
                    </div>
                    <div class="caseStudy__info__box__header">
                        <h2>Contract Duration</h2>
                    </div>
                    <div class="caseStudy__info__box__text">
                        <h5>18 months</h5>
                    </div>
                </div>
                <div class="caseStudy__info__box">
                    <div class="caseStudy__info__box__icon">
                        <span><i class="fas fa-map-marker-alt"></i></span>
                    </div>
                    <div class="caseStudy__info__box__header">
                        <h2>Location</h2>
                    </div>
                    <div class="caseStudy__info__box__text">
                        <h5>St Albans, Hertfordshire AL1 3AR</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--
    <section class="caseStory">
        <div class="container">
            <div class="caseStory__header">

                <div class="caseStory__header__card">
                    <div class="caseStory__header__card__body">
                        <div class="caseStory__header__card__body__text">
                            <div>
                                <h5 class="navyBlue">Information</h5>
                            </div>
                            <div>
                                <p><span class="bold">Client:</span> Higgins Construction</p>
                            </div>
                            <div>
                                <p><span class="bold">Contract Value: </span>£6,300,000</p>
                            </div>
                            <div>
                                <p><span class="bold">Contract Duration:</span> 18 months</p>
                            </div>
                            <div>
                                <p><span class="bold">Location:</span> St Albans, Hertfordshire AL1 3AR</p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="caseStory__header__image">
                    <img src="assets/img/image--29.jpg">
                </div>
            </div>

        </div>
    </section>
-->




    <footer>
        <div class="footer__section1">
            <div class="footer__container">
                <div class="footer__section1__header">
                    <h5>Contact Us</h5>
                </div>
                <div class="footer__section1__links">
                    <ul class="footer">
                        <li class="footer"><a href="index.php#ourCompanies__section">Our Family of Companies</a></li>
                        <li class="footer"><a href="whatwedo.php">What We Do</a></li>
                        <li class="footer"><a href="casestudies.php">case studies</a></li>
                        <li class="footer"><a href="aboutus.php">About Us</a></li>
                        <li class="footer"><a href="contact.php">contact</a></li>
                    </ul>
                </div>
                <div class="footer__section1__contact">
                    <div class="footer__section1__contact__link1">
                        <div class="footer__section1__contact__link1__logo">
                            <span><i class="fas fa-envelope"></i></span>
                        </div>
                        <div class="footer__section1__contact__link1__text">
                            <a href="mailto:info@mimramservices.com">
                                <p>info@mimramservices.com</p>
                            </a>
                        </div>
                    </div>
                    <div class="footer__section1__contact__link2">
                        <div class="footer__section1__contact__link2__logo">
                            <span><i class="fas fa-phone-alt"></i></span>
                        </div>
                        <div class="footer__section1__contact__link2__text">
                            <a href="tel:01582 568212">
                                <p>01582 568212</p>
                            </a>
                        </div>
                    </div>
                    <div class="footer__section1__contact__link3">
                        <div class="footer__section1__contact__link3__logo">
                            <span><i class="fas fa-map-marker-alt"></i></span>
                        </div>
                        <div class="footer__section1__contact__link3__text">
                            <p>Unit 27 Sundon Industrial Estate, Dencora Way, Bedfordshire, LU3 3HP</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="footer__section2">
            <div class="footer__button">
                <a href="contact.php">
                    <button class="contact"><span>Contact Mimram</span></button>
                </a>
            </div>
            <div class="footer__section2__logo">
                <div class="footer__section2__logo__image">
                    <div class="footer__section2__logo__image__logo">
                        <img src="assets/img/logo.png">
                    </div>
                </div>
                <div class="footer__section2__logo__text">
                    <p>Efficient, effective engineering solutions</p>
                </div>
            </div>
            <div class="footer__section2__links">
                <div class="footer__section2__links">
                    <ul class="footer">
                        <li class="footer footerRight"><a class="footerRight">COPYRIGHT 2020</a></li>
                        <li class="footer"><a href="privacy.php" class="footerRight">PRIVACY</a></li>
                        <li class="footer"><a href="terms.php" class="footerRight">TERMS</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <script type="text/javascript" src="assets/js/slick/slick.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js"></script>

</body>

</html>
