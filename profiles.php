<!doctype html>

<html lang="en">

<head>
    <meta charset="utf-8">

    <title>Profiles - Mimram</title>

    <meta name="description" content="Mimram">
    <meta name="author" content="SitePoint">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick.css" />
    <link rel="stylesheet" href="dist/css/main.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/fd007505c4.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>



</head>

<body>
    <header class="header">
        <div class="header__wrapper">
            <div class="header__logo logo">
                <a href="index.php"><img src="assets/img/logo-trans.png" /></a>
            </div>
            <div class="header__nav nav">
                <ul>


                    <li>
                        <a href="index.php#ourCompanies__section">our family of companies</a>
                        <ul>
                            <li>
                                <a href="ponspecial.php">PON Special Works</a>
                            </li>
                            <li>
                                <a href="mimramengineering.php">Mimram Engineering Service</a>
                            </li>
                            <li>
                                <a href="mimramspecial.php">Mimram Special Works</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="whatwedo.php">what we do</a>
                        <ul>
                            <li>
                                <a href="whatwedo.php#our_services">Our Services</a>
                            </li>
                            <li>
                                <a href="whatwedo.php#our_commitments">Our Commitments</a>
                            </li>
                            <li>
                                <a href="whatwedo.php#our_quality">Quality</a>
                            </li>
                            <li>
                                <a href="whatwedo.php#our_accreditations">Accreditations</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="casestudies.php">case studies</a>

                    </li>
                    <li>
                        <a href="aboutus.php">about us</a>
                        <ul>
                            <li>
                                <a href="aboutus.php">About Us</a>
                            </li>
                            <li>
                                <a href="values.php">Our Values</a>
                            </li>
                            <li>
                                <a href="profiles.php">Profiles</a>
                            </li>
                            <li>
                                <a href="history.php">Our History</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="contact.php">contact</a></li>
                </ul>
            </div>
            <div class="header__contact contact">
                <ul>
                    <li>
                        <a href="mailto:info@mimramservices.com"><i class="fas fa-envelope"></i>
                            info@mimramservices.com</a>
                    </li>
                    <li>
                        <a href="tel:01582 568212"><i class="fas fa-phone-alt"></i> 01582 568212</a>
                    </li>
                </ul>
            </div>
            <div class="header__hamburger">
                <div id="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
    </header>


    <section class="header__images">
        <div class="header__image"> <img src="assets/img/image--27--big.jpg"> </div>
    </section>

    <section class="profiles">
        <div class="container">
            <div class="companies__header">
                <h1>Profiles</h1>
            </div>
            <div class="profiles__list">
                <div class="profiles__list__row">
                    <div class="profiles__list__person">
                        <div class="profiles__list__person__name">
                            <h4>Gary O'Neill</h4>
                        </div>
                        <div class="profiles__list__person__title">
                            <h5>Managing Director</h5>
                        </div>
                        <div class="profiles__list__person__info">
                            <p>As Managing Director, Gary has been in the industry for 30+ years and has experience in
                                many sectors within both the built environment and industry. <br><br>
                                Since purchasing Mimram in 2003, Gary has broadened its path from its original operating
                                base in the Temporary Site Services industry to a fully incorporating building services
                                engineering company offering Electrical & Mechanical Engineering services to a key
                                client base. Many of whom are considered key players in the UK construction industry.
                                Under Gary’s stewardship Mimram has progressed with steady and controlled growth not
                                only building lasting relationships with his employees and key members of the business
                                but also establishing strong partnerships with clients. <br><br>
                                <span class="italics">
                                    “It’s not about just understanding the engineering, that’s the easy bit. We need to
                                    not only understand our client’s requirements but form a partnership based on
                                    reliability, transparency and trust, that is the key. If we do this we are able to
                                    deliver the right results. That brings us back together time and time again. This is
                                    where we all win, a common understanding.”</span><br><br>
                                Gary’s background is in building services engineering principally electrical but he has
                                a broad knowledge of most of the services required in today’s built environment. Gary
                                believes that a key element of what is required of today’s engineering services partners
                                is an appreciation that every project has its own unique requirement no matter how large
                                or small. During his time in the engineering services sector Gary has found that there
                                is always one common denominator no matter the sector, and this is the need for high
                                quality engineering services partner.
                            </p>
                        </div>
                    </div>
                    <div class="profiles__list__person lastPerson">
                        <div class="profiles__list__person__name">
                            <h4>Nathan Meakins</h4>
                        </div>
                        <div class="profiles__list__person__title">
                            <h5>SHEQ &amp; Contract Manager</h5>
                        </div>
                        <div class="profiles__list__person__info">
                            <p>With over 21 years’ experience within the M&amp;E industry as well as a qualified
                                specialist in Plumbing, Heating, Ventilation and Air conditioning with vast knowledge
                                and experience in all aspects of plumbing. A hands on former Project manager with
                                5 years’ extensive experience within the residential, commercial, industrial, health
                                and Education industries, Nathan’s responsibilities as contracts manager include
                                managing the general operations and overlooking estimating, design, tendering,
                                work scheduling and the overall and complete management of projects from
                                conception to completion.<br><br>
                                As one of the Company’s Health &amp; Safety Representatives and IOSH certified,
                                Nathan is also responsible for the overall Health &amp; Safety for the organisation.
                            </p>
                        </div>
                    </div>


                </div>
                <div class="profiles__list__row">
                    <div class="profiles__list__person">
                        <div class="profiles__list__person__name">
                            <h4>Hamina Vadgama</h4>
                        </div>
                        <div class="profiles__list__person__title">
                            <h5>Human Resources Manager</h5>
                        </div>
                        <div class="profiles__list__person__info">
                            <p>With over 8 years’ experience in Human Resources and a member of CIPD, Hamina
                                has experience in generalist Human Resources Management and specialises in
                                aligning stakeholder expectations with business needs. With core focus on employee
                                retainment and upskilling, welfare, conflict resolution, Company policy and
                                Procedures, Data Protection and generalist Human Resources.<br><br>
                                Hamina supports Company marketing requirements as well as driving core company
                                values and communication throughout the culture of the business.<br><br>
                                As one of the Company Health &amp; Safety Representatives and IOSH certified,
                                Hamina is also responsible for the overall Health &amp; Safety of the organisation.
                            </p>
                        </div>
                    </div>
                    <div class="profiles__list__person ">
                        <div class="profiles__list__person__name">
                            <h4>Sarah Ward</h4>
                        </div>
                        <div class="profiles__list__person__title">
                            <h5>Finance Director</h5>
                        </div>
                        <div class="profiles__list__person__info">
                            <p>With 15 years’ experience in Finance and qualified in NVQ 3 Business & Administration,
                                Sarah specialises in financial reporting critical account and finance information to
                                stakeholders and cash management. Sarah oversees and manages all aspects of finance
                                within the company as well as managing her own departmental team.
                            </p>
                        </div>
                    </div>
                    <div class="profiles__list__person lastPerson">
                        <div class="profiles__list__person__name">
                            <h4>Arlinda Stafa</h4>
                        </div>
                        <div class="profiles__list__person__title">
                            <h5>Quantity Surveyor</h5>
                        </div>
                        <div class="profiles__list__person__info ">
                            <p>Having worked within the M&amp;E industry for 5 years and with 2.5 years’ experience in
                                quantity surveying, Arlinda is responsible for the Surveying and Commercial
                                obligations of the Company, specifically in commercial reporting and Cost &amp; Budget
                                analysis, identifying, complying and agreeing variations through consistent client
                                liaison, Payment applications, assigning, assessing and managing sub-contractor
                                packages strictly in accordance with their terms and conditions, contract negotiation,
                                production of contract administration documents and the overall contractual
                                obligations.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="footer__section1">
            <div class="footer__container">
                <div class="footer__section1__header">
                    <h5>Contact Us</h5>
                </div>
                <div class="footer__section1__links">
                    <ul class="footer">
                        <li class="footer"><a href="index.php#ourCompanies__section">Our Family of Companies</a></li>
                        <li class="footer"><a href="whatwedo.php">What We Do</a></li>
                        <li class="footer"><a href="casestudies.php">case studies</a></li>
                        <li class="footer"><a href="aboutus.php">About Us</a></li>
                        <li class="footer"><a href="contact.php">contact</a></li>
                    </ul>
                </div>
                <div class="footer__section1__contact">
                    <div class="footer__section1__contact__link1">
                        <div class="footer__section1__contact__link1__logo">
                            <span><i class="fas fa-envelope"></i></span>
                        </div>
                        <div class="footer__section1__contact__link1__text">
                            <a href="mailto:info@mimramservices.com">
                                <p>info@mimramservices.com</p>
                            </a>
                        </div>
                    </div>
                    <div class="footer__section1__contact__link2">
                        <div class="footer__section1__contact__link2__logo">
                            <span><i class="fas fa-phone-alt"></i></span>
                        </div>
                        <div class="footer__section1__contact__link2__text">
                            <a href="tel:01582 568212">
                                <p>01582 568212</p>
                            </a>
                        </div>
                    </div>
                    <div class="footer__section1__contact__link3">
                        <div class="footer__section1__contact__link3__logo">
                            <span><i class="fas fa-map-marker-alt"></i></span>
                        </div>
                        <div class="footer__section1__contact__link3__text">
                            <p>Unit 27 Sundon Industrial Estate, Dencora Way, Bedfordshire, LU3 3HP</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="footer__section2">
            <div class="footer__button">
                <a href="contact.php">
                    <button class="contact"><span>Contact Mimram</span></button>
                </a>
            </div>
            <div class="footer__section2__logo">
                <div class="footer__section2__logo__image">
                    <div class="footer__section2__logo__image__logo">
                        <img src="assets/img/logo.png">
                    </div>
                </div>
                <div class="footer__section2__logo__text">
                    <p>Efficient, effective engineering solutions</p>
                </div>
            </div>
            <div class="footer__section2__links">
                <div class="footer__section2__links">
                    <ul class="footer">
                        <li class="footer footerRight"><a class="footerRight">COPYRIGHT 2020</a></li>
                        <li class="footer"><a href="privacy.php" class="footerRight">PRIVACY</a></li>
                        <li class="footer"><a href="terms.php" class="footerRight">TERMS</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <script type="text/javascript" src="assets/js/slick/slick.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js"></script>

</body>

</html>
