<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />

    <title>Home - Mimram</title>

    <meta name="description" content="Mimram" />
    <meta name="author" content="SitePoint" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />

    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick.css" />
    <link rel="stylesheet" href="dist/css/main.min.css" />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet" />
    <script src="https://kit.fontawesome.com/fd007505c4.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
</head>

<body>
    <header class="header">
        <div class="header__wrapper">
            <div class="header__logo logo">
                <a href="index.php"><img src="assets/img/logo-trans.png" /></a>
            </div>
            <div class="header__nav nav">
                <ul>


                    <li>
                        <a href="javascript:scrollCompany();">our family of companies</a>
                        <ul>
                            <li>
                                <a href="ponspecial.php">PON Special Works</a>
                            </li>
                            <li>
                                <a href="mimramengineering.php">Mimram Engineering Service</a>
                            </li>
                            <li>
                                <a href="mimramspecial.php">Mimram Special Works</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="whatwedo.php">what we do</a>
                        <ul>
                            <li>
                                <a href="whatwedo.php#our_services">Our Services</a>
                            </li>
                            <li>
                                <a href="whatwedo.php#our_commitments">Our Commitments</a>
                            </li>
                            <li>
                                <a href="whatwedo.php#our_quality">Quality</a>
                            </li>
                            <li>
                                <a href="whatwedo.php#our_accreditations">Accreditations</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="casestudies.php">case studies</a>

                    </li>
                    <li>
                        <a href="aboutus.php">about us</a>
                        <ul>
                            <li>
                                <a href="aboutus.php">About Us</a>
                            </li>
                            <li>
                                <a href="values.php">Our Values</a>
                            </li>
                            <li>
                                <a href="profiles.php">Profiles</a>
                            </li>
                            <li>
                                <a href="history.php">Our History</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="contact.php">contact</a></li>
                </ul>
            </div>
            <div class="header__contact contact">
                <ul>
                    <li>
                        <a href="mailto:info@mimramservices.com"><i class="fas fa-envelope"></i>
                            info@mimramservices.com</a>
                    </li>
                    <li>
                        <a href="tel:01582 568212"><i class="fas fa-phone-alt"></i> 01582 568212</a>
                    </li>
                </ul>
            </div>
            <div class="header__hamburger">
                <div id="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
    </header>
    <div class="banner">
        <div class="banner__main">
            <div class="banner__main__arrows">
                <div class="arrow_prev__banner arrow__banner">
                    <img src="assets/img/arrow--left--small.png" />
                </div>
                <div class="arrow_next__banner arrow__banner">
                    <img src="assets/img/arrow--right.png" />
                </div>

            </div>
            <div class="banner__slider">
                <div class="slider__content">
                    <div class="slider__items">
                        <div class="slide1">
                            <img src="assets/img/background1.jpg" />
                            <div class="banner__card">
                                <div class="banner__card__header">
                                    <h3><span class='navyBlue'>Family owned with a reputation for ethical and efficient construction.</span> </h3>
                                </div>

                                <div class="banner__card__subheader">
                                    <h4>We remain family-owned and the people we employ, our people are what make us different.</h4>
                                </div>
                                <div class="banner__card__text">
                                    <h5>
                                        <h5>We aim to make a <span class='navyBlue bold'>positive difference</span>, including how we source, plan and use materials, whilst <span class='navyBlue'>minimising waste.</span> We <span class='navyBlue bold'>invest</span> in our people encouraging <span class='navyBlue bold'>innovative and sustainable solutions</span>, which has helped us maintain successful working relationships with a <span class='navyBlue bold'>large client base.</span></h5>
                                    </h5>
                                </div>
                                <div class="banner__card__link">
                                    <a href="aboutus.php"><span class="bold">Read more about us >></span></a>
                                </div>
                            </div>
                        </div>
                        <div class="slide2">
                            <img src="assets/img/background2.jpg" />
                            <div class="banner__card border-orange">
                                <div class="banner__card__header">
                                    <h3>
                                        <span class='navyBlue'>
                                            <h3>Success built around our people, the environment and building a better place.</h3>
                                        </span>
                                    </h3>
                                </div>

                                <div class="banner__card__subheader">
                                    <h4>We like to do things differently and in a way that enhances a modern world.</h4>
                                </div>
                                <div class="banner__card__text">
                                    <h5>
                                        <h5>Established in 1985, we are a, specialist design and build, Mechanical & Electrical contractor. We’ve built our company around <span class='orange bold'>our people</span>, encouraging the development of solutions that <span class='orange bold'>help the environment rather than destroying it</span>. We adhere to a <span class='orange bold'>strong company ethos</span>, in that we care for clients, the environment and the people that work with us.</h5>
                                    </h5>
                                </div>
                                <div class="banner__card__link">
                                    <a href="index.php#ourCompanies__section"><span class="bold">View our companies >></span></a>
                                </div>
                            </div>
                        </div>
                        <div class="slide3">
                            <img src="assets/img/background3.jpg" />
                            <div class="banner__card border-red">
                                <div class="banner__card__header">
                                    <h3><span class='red'>Amongst our extensive client base we have completed various projects over the years.</span></h3>
                                </div>

                                <div class="banner__card__subheader">
                                    <h4>Often offering whole package solutions and logistics we are used to adapting to tight time frames and cost restraints.</h4>
                                </div>
                                <div class="banner__card__text">
                                    <h5>We frequently come up against a raft of <span class='red'>challenges</span> whereby we are ensuring a design brief is fulfilled and various <span class='red'>sector standards</span> are adhered to and met. Full installations often require not just construction but also full <span class='red'>mechanical and electrical services</span> alongside <span class='red'>technical compliance.</span></h5>
                                </div>
                                <div class="banner__card__link">
                                    <a href="casestudies.php"><span class="bold">View our case studies here >></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- <div class="banner__empty"></div> -->
    </div>

    <section class="type1">
        <div class="section__type1">
            <div class="section__type1__main">
                <div class="section__type1__left">
                    <div class="container">
                        <div class="logo">
                            <a href="index.php"><img src="assets/img/logolq.png" /></a>
                        </div>
                        <div class="text">
                            <h1>Over 40</h1>
                            <h3>years experience</h3>
                        </div>
                    </div>
                </div>
                <div class="section__type1__right">
                    <div class="container">
                        <h4>
                            Committed to innovation we take pride in our delivery of a full
                            range of Mechanical & Electrical services that are on time, in
                            spec and within budget.
                        </h4>
                        <p>
                            What sets us apart from others is our people. We believe in a
                            trust based culture and core values, allowing our team to not
                            only manage projects but also manage relationships with our
                            clients that are long term and ones that are built on
                            expectation and reliability.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="inform">
        <div class="inform__leftSide">
            <div class="inform__leftSide__picture">
                <img src="assets/img/section1mainpic.png" />
            </div>
            <div class="inform__leftSide__section1 skyBlue">
                <div class="inform__leftSide__section1__content">
                    <div class="container">
                        <h2 class="navyBlue">Services</h2>
                        <p>Our strength as a business lies within our wealth of experience and skills held by our people. They are highly experienced staff who maintain independent and specialist knowledge to the services we offer. </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="inform__rightSide">
            <div class="inform__rightSide__section1 darkBlue">
                <div class="inform__rightSide__section1__content">
                   <div class="container">
                    <h2>Mechanical</h2>
                        <p>
                            For residential, commercial or retail projects, our dedicated
                            and multidiscipline team of plumbers, fitters, gas safe
                            engineers and refcom engineers have a vast depth in knowledge
                            and skill. </p>

                        <div id="moreMechanical">
                            <p>They provide a full range of plumbing, drainage and
                                heating solutions, for all sizes of mechanical installations. As
                                an approved Contractor with leading manufacturers our team have
                                built the reputation on high quality and standards. This varies
                                from the basics of gas, water and drainage, through to solutions
                                that comply with regulatory challenges, our team offer solutions
                                that are price competitive and cost effective.
                            </p>
                            <ul class="circle">
                                <li class="circle"><a>Pipework</a></li>
                                <li class="circle"><a>Design install</a></li>
                                <li class="circle">
                                    <a>Domestic and Commercial gas installation</a>
                                </li>
                                <li class="circle"><a>Above ground drainage</a></li>
                                <li class="circle"><a>LTHW Systems</a></li>
                                <li class="circle"><a>Central Plant</a></li>
                                <li class="circle"><a>Rainwater Services</a></li>
                                <li class="circle"><a>Fabrication works</a></li>
                                <li class="circle"><a>Ventilation Systems</a></li>
                                <li class="circle"><a>Air conditioning</a></li>
                                <li class="circle"><a>Underfloor Heating Systems</a></li>
                            </ul>
                        </div>
                        <button id="showMechanical" class="readMoreBtn">Read More >>></button>
                </div>
                <div class="inform__rightSide__section1__border"></div>
            </div>
            </div>
            <div class="inform__rightSide__section2">
                <div class="inform__rightSide__section2__content">
                    <div class="container">
                        <h2 class="navyBlue">Electrical</h2>
                        <p>
                            Electrical installations are business-critical services. From
                            emergency lighting and power systems through to bespoke
                            uninterruptable power distribution systems. We have delivered
                            landmark installations which we pride ourselves on. </p>
                        <div id="moreElectrical">
                            <p>Not only
                                providing our service but working in collaboration with our
                                clients to align their visions and ensure customer satisfaction
                                with quality and engineering expertise.
                            </p>
                            <ul class="circle">
                                <li class="circle"><a>AV Solutions</a></li>
                                <li class="circle"><a>Low Voltage</a></li>
                                <li class="circle"><a>Small Power Installation</a></li>
                                <li class="circle">
                                    <a>Lighting (internal, External & Emergency)</a>
                                </li>
                                <li class="circle"><a>Multi-failsafe UPS </a></li>
                                <li class="circle"><a>CCTV </a></li>
                                <li class="circle">
                                    <a>Intruder Alarm and Access Systems </a>
                                </li>
                                <li class="circle">
                                    <a>Fire detection Systems, Fire Detection </a>
                                </li>
                                <li class="circle"><a>Home Automation</a></li>
                                <li class="circle"><a>Electrical Testing</a></li>
                                <li class="circle"><a>PAT Testing</a></li>
                            </ul>
                        </div>
                        <button id="showElectrical" class="readMoreBtn">Read More >>></button>
                    </div>
                </div>
            </div>
            <div class="inform__rightSide__section3">
                <div class="inform__rightSide__section3__content">
                    <div class="container">
                        <h2>Design & Build</h2>
                        <p>
                            Our dynamic, in-house engineering team has the full range of
                            contemporary design technologies at its fingertips. We not only
                            focus on simplifying project management but help to keep the
                            budget on track too, using craft solutions that are cost
                            effective and energy efficient.
                        </p>
                    </div>
                </div>
                <div class="inform__rightSide__section3__button">
                    <a href="contact.php">
                        <button class="contact"><span>Contact Mimram</span></button>
                    </a>
                </div>
            </div>
            <div class="inform__rightSide__section4">
                <div class="inform__rightSide__section4__content">
                    <div class="container"></div>
                </div>
            </div>
        </div>
    </section>

    <section class="splitImages">
        <div class="splitImages__1">
            <img src="assets/img/Architecture.jpg" />
            <div class="splitImages__card">
                <div class="splitImages__card__quote">
                    <i class="fas fa-quote-left"></i>
                </div>
                <div class="splitImages__card__text">
                    <h5>
                        We care for clients, the environment and the people that work with us.
                    </h5>
                </div>
            </div>
        </div>
        <div class="splitImages__2">
            <img src="assets/img/constrution.PNG" />
        </div>
    </section>

    <section class="ourCompanies" id="ourCompanies__section">
        <div class="ourCompanies__header">
            <h2><span class="navyBlue">Our COMPANIES</span></h2>
        </div>
        <div id="ourCompanies__slider">
            <div class="container">
                <div class="slick__content">
                    <div class="slick__items">
                        <div class="test"><a href="mimramengineering.php"><img src="assets/img/logo--1.png" /></a></div>
                        <div class="test"><a href="ponspecial.php"><img src="assets/img/logo--2.png" /></a></div>
                        <div class="test"><a href="mimramspecial.php"><img src="assets/img/logo--3.png" /></a></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sectors">
        <div class="container reducedContainer">
            <div class="sectors__header">
                <h2 class="navyBlue">Sectors</h2>
            </div>
            <div class="sectors__left__container">
                <div class="sectors__left">
                    <div class="sectors__infobox">

                        <div class="sectors__infobox__body">
                            <div class="color_overlay"></div>
                            <div class="sectors__infobox__body__background">
                                <img src="assets/img/gabriel.jpg">

                            </div>
                            <div class="sectors__infobox__body__content">
                                <div class="sectors__infobox__body__header">
                                    <h4>Commercial</h4>
                                </div>
                                <div class="sectors__infobox__body__text">
                                    <p>Offering efficient and practical solutions to projects of all shapes and sizes; from new buildings, large and refurbished office projects to high end retail outlets.</p>
                                    <a href="fattorie.php">Read More about our experience in Commercial >></a>
                                </div>
                            </div>
                        </div>
                        <div class="sectors__infobox__body">
                           <div class="color_overlay"></div>
                            <div class="sectors__infobox__body__background">
                                <img src="assets/img/marsh2.jpg">
                            </div>
                            <div class="sectors__infobox__body__content">
                                <div class="sectors__infobox__body__header">
                                    <h4>Residential</h4>
                                </div>
                                <div class="sectors__infobox__body__text">
                                    <p>From basic refits, to a high end multimillion pound flat complexes, our teams partner up with clients from start to finish ensuring the success of every project.</p>
                                    <a href="engie.php">Read More about our experience in Residential >></a>
                                </div>
                            </div>
                        </div>
                        <div class="sectors__infobox__body">
                           <div class="color_overlay"></div>
                            <div class="sectors__infobox__body__background">
                                <img src="assets/img/case_no1__2.jpg">
                            </div>
                            <div class="sectors__infobox__body__content">
                                <div class="sectors__infobox__body__header">
                                    <h4>Industrial</h4>
                                </div>
                                <div class="sectors__infobox__body__text">
                                    <p>Our team have experience in completing an immense variety of industrial projects such as engineering plants, rubber moulding plants, paint shops, etc. They understand the requirements to ensure utilisation and all challenges are met.</p>
                                    <a href="ibc.php">Read More about our experience in Industrial >></a>
                                </div>
                            </div>
                        </div>
                        <div class="sectors__infobox__body">
                           <div class="color_overlay"></div>
                            <div class="sectors__infobox__body__background">
                                <img src="assets/img/fattorie3.jpg">
                            </div>
                            <div class="sectors__infobox__body__content">
                                <div class="sectors__infobox__body__header">
                                    <h4>Education</h4>
                                </div>
                                <div class="sectors__infobox__body__text">
                                    <p>Acknowledging the need to inspire the future of tomorrow; our team collaborate with clients to ensure a secure, trusted and comfortable environment is provided to children and adults. Where knowledge can be developed and shared.</p>
                                    <a href="morgan.php">Read More about our experience in Education >></a>
                                </div>
                            </div>
                        </div>
                        <div class="sectors__infobox__body">
                           <div class="color_overlay"></div>
                            <div class="sectors__infobox__body__background">
                                <img src="assets/img/image--16.jpg">
                            </div>
                            <div class="sectors__infobox__body__content">
                                <div class="sectors__infobox__body__header">
                                    <h4>Healthcare</h4>
                                </div>
                                <div class="sectors__infobox__body__text">
                                    <p>Helping to provide tailored healthcare solutions to the sector. Whether Pharmaceuticals, Care/ Residential homes or Hospitals, we understand the necessity for each project, to fit the needs of patient requirements, NHS professionals and the surrounding communities.</p>
                                    <a href="imtech.php">Read More about our experience in Healthcare >></a>
                                </div>
                            </div>
                        </div>
                        <div class="sectors__infobox__body">
                           <div class="color_overlay"></div>
                            <div class="sectors__infobox__body__background">
                                <img src="assets/img/image--24.jpg">
                            </div>
                            <div class="sectors__infobox__body__content">
                                <div class="sectors__infobox__body__header">
                                    <h4>Sports & Leisure</h4>
                                </div>
                                <div class="sectors__infobox__body__text">
                                    <p>As one of the largest and fastest growing sectors, we understand the need of project completion within a cost efficient and short time frame. To keep up with the constant progression as new activities and equipment are developed, we help clients to fulfil requirements without hindering individual and community needs for achieving personal and health goals.</p>
                                    <a href="lovegrove.php">Read More about our experience in Sports & Leisure >></a>
                                </div>
                            </div>
                        </div>
                        <div class="sectors__infobox__body">
                           <div class="color_overlay"></div>
                            <div class="sectors__infobox__body__background">
                                <img src="assets/img/image--25.jpg">
                            </div>
                            <div class="sectors__infobox__body__content">
                                <div class="sectors__infobox__body__header">
                                    <h4>Aviation</h4>
                                </div>
                                <div class="sectors__infobox__body__text">
                                    <p>From Fuel Farms to Airside/Landside our team have over 20 years’ experience working within the aviation industry ensuring a seamless delivery of expectations. We are familiar with the sensitivity that is required within the aviation environment.</p>
                                    <a href="luton.php">Read More about our experience in Aviation >></a>
                                </div>
                            </div>
                        </div>










                        <!--<div class="sectors__infobox__left">
                            <div class="sectors__infobox__box">
                                <div class="sectors__infobox__box__header">
                                    <div class="sectors__infobox__box__header__logo">
                                        <img src="assets/img/logolq.png">
                                    </div>
                                    <div class="sectors__infobox__box__header__text">
                                        <h5 class="navyBlue bold">Commercial</h5>
                                    </div>
                                </div>

                                <p>Offering efficient and practical solutions to projects of all shapes and sizes; from new buildings, large and refurbished office projects to high end retail outlets.</p>
                            </div>
                            <div class="sectors__infobox__box">
                                <div class="sectors__infobox__box__header">
                                    <div class="sectors__infobox__box__header__logo">
                                        <img src="assets/img/logolq.png">
                                    </div>
                                    <div class="sectors__infobox__box__header__text">
                                        <h5 class="navyBlue bold">Residential</h5>
                                    </div>
                                </div>
                                <p>From basic refits, to a high end multimillion pound flat complexes, our teams partner up with clients from start to finish ensuring the success of every project.</p>
                            </div>
                            <div class="sectors__infobox__box">
                                <div class="sectors__infobox__box__header">
                                    <div class="sectors__infobox__box__header__logo">
                                        <img src="assets/img/logolq.png">
                                    </div>
                                    <div class="sectors__infobox__box__header__text">
                                        <h5 class="navyBlue bold">Industrial</h5>
                                    </div>
                                </div>
                                <p>Our team have experience in completing an immense variety of industrial projects such as engineering plants, rubber moulding plants, paint shops, etc. They understand the requirements to ensure utilisation and all challenges are met.</p>
                            </div>
                            <div class="sectors__infobox__box">
                                <div class="sectors__infobox__box__header">
                                    <div class="sectors__infobox__box__header__logo">
                                        <img src="assets/img/logolq.png">
                                    </div>
                                    <div class="sectors__infobox__box__header__text">
                                        <h5 class="navyBlue bold">Education</h5>
                                    </div>
                                </div>
                                <p>Acknowledging the need to inspire the future of tomorrow; our team collaborate with clients to ensure a secure, trusted and comfortable environment is provided to children and adults. Where knowledge can be developed and shared</p>
                            </div>
                        </div>
                        <div class="sectors__infobox__right">
                            <div class="sectors__infobox__box">
                                <div class="sectors__infobox__box__header">
                                    <div class="sectors__infobox__box__header__logo">
                                        <img src="assets/img/logolq.png">
                                    </div>
                                    <div class="sectors__infobox__box__header__text">
                                        <h5 class="navyBlue bold">Healthcare</h5>
                                    </div>
                                </div>
                                <p>Helping to provide tailored healthcare solutions to the sector. Whether Pharmaceuticals, Care/ Residential homes or Hospitals, we understand the necessity for each project, to fit the needs of patient requirements, NHS professionals and the surrounding communities.</p>
                            </div>
                            <div class="sectors__infobox__box">
                                <div class="sectors__infobox__box__header">
                                    <div class="sectors__infobox__box__header__logo">
                                        <img src="assets/img/logolq.png">
                                    </div>
                                    <div class="sectors__infobox__box__header__text">
                                        <h5 class="navyBlue bold">Sports & Leisure</h5>
                                    </div>
                                </div>
                                <p>As one of the largest and fastest growing sectors, we understand the need of project completion within a cost efficient and short time frame. To keep up with the constant progression as new activities and equipment are developed, we help clients to fulfil requirements without hindering individual and community needs for achieving personal and health goals.</p>
                            </div>
                            <div class="sectors__infobox__box">
                                <div class="sectors__infobox__box__header">
                                    <div class="sectors__infobox__box__header__logo">
                                        <img src="assets/img/logolq.png">
                                    </div>
                                    <div class="sectors__infobox__box__header__text">
                                        <h5 class="navyBlue bold">Aviation</h5>
                                    </div>
                                </div>
                                <p>From Fuel Farms to Airside/Landside our team have over 20 years’ experience working within the aviation industry ensuring a seamless delivery of expectations. We are familiar with the sensitivity that is required within the aviation environment.</p>
                            </div>
                        </div>-->

                    </div>
                </div>

            </div>
            <!--<div class="sectors__right">
                <div class="sectors__img1">
                    <img src="assets/img/sector__1.PNG" />
                </div>
                <div class="sectors__img2">
                    <img src="assets/img/sector__2.PNG" />
                </div>
            </div>-->
        </div>
    </section>
    <section class="accreditations">
        <div class="container reducedContainer__border">
            <div class="accreditations__card__1">
                <div class="accreditations__card__1__image">
                    <img src="assets/img/iso.png" />
                </div>
            </div>

            <div class="accreditations__card__2">
                <div class="accreditations__card__quote">
                    <i class="fas fa-quote-left"></i>
                </div>
                <div class="accreditations__card__text">
                    <h5>
                        Success built around our people, the environment and building a
                        better place
                    </h5>
                </div>
            </div>
        </div>


        <div class="accreditations_slider">
            <div class="container reducedContainer">
                <h2 class="navyBlue">Our Accreditations</h2>
                <div class="arrow_next arrow_alternate">
                    <img src="assets/img/arrow--right.png">
                </div>
                <div class="accreditation_row">
                    <div class="accreditation"><img src="assets/img/logo1.png"></div>
                    <div class="accreditation"><img src="assets/img/logo2.png"></div>
                    <div class="accreditation"><img src="assets/img/logo3.png"></div>
                    <div class="accreditation"><img src="assets/img/200.png"></div>
                    <div class="accreditation"><img src="assets/img/200.png"></div>
                    <div class="accreditation"><img src="assets/img/200.png"></div>
                </div>
                <div class="arrow_prev arrow_alternate">
                    <img src="assets/img/arrow--left.png">
                </div>
                <div class="accreditation_button">
                    <a href="whatwedo.php#our_accreditations"><button class="contact">Read More</button></a>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="footer__section1">
            <div class="footer__container">
                <div class="footer__section1__header">
                    <h5>Contact Us</h5>
                </div>
                <div class="footer__section1__links">
                    <ul class="footer">
                        <li class="footer"><a href="index.php#ourCompanies__section">Our Family of Companies</a></li>
                        <li class="footer"><a href="whatwedo.php">What We Do</a></li>
                        <li class="footer"><a href="casestudies.php">case studies</a></li>
                        <li class="footer"><a href="aboutus.php">About Us</a></li>
                        <li class="footer"><a href="contact.php">contact</a></li>
                    </ul>
                </div>
                <div class="footer__section1__contact">
                    <div class="footer__section1__contact__link1">
                        <div class="footer__section1__contact__link1__logo">
                            <span><i class="fas fa-envelope"></i></span>
                        </div>
                        <div class="footer__section1__contact__link1__text">
                            <a href="mailto:info@mimramservices.com">
                                <p>info@mimramservices.com</p>
                            </a>
                        </div>
                    </div>
                    <div class="footer__section1__contact__link2">
                        <div class="footer__section1__contact__link2__logo">
                            <span><i class="fas fa-phone-alt"></i></span>
                        </div>
                        <div class="footer__section1__contact__link2__text">
                            <a href="tel:01582 568212">
                                <p>01582 568212</p>
                            </a>
                        </div>
                    </div>
                    <div class="footer__section1__contact__link3">
                        <div class="footer__section1__contact__link3__logo">
                            <span><i class="fas fa-map-marker-alt"></i></span>
                        </div>
                        <div class="footer__section1__contact__link3__text">
                            <p>
                                Unit 27 Sundon Industrial Estate, Dencora Way, Bedfordshire,
                                LU3 3HP
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__section2">
            <div class="footer__button">
                <a href="contact.php">
                    <button class="contact"><span>Contact Mimram</span></button>
                </a>
            </div>
            <div class="footer__section2__logo">
                <div class="footer__section2__logo__image">
                    <div class="footer__section2__logo__image__logo">
                        <img src="assets/img/logo.png" />
                    </div>
                </div>
                <div class="footer__section2__logo__text">
                    <p>Efficient, effective engineering solutions</p>
                </div>
            </div>
            <div class="footer__section2__links">
                <div class="footer__section2__links">
                    <ul class="footer">
                        <li class="footer footerRight">
                            <a href="#" class="footerRight">COPYRIGHT 2020</a>
                        </li>
                        <li class="footer">
                            <a href="#" class="footerRight">PRIVACY</a>
                        </li>
                        <li class="footer"><a href="terms.php" class="footerRight">TERMS</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <script type="text/javascript" src="assets/js/slick/slick.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="assets/js/main.js"></script>
</body>

</html>
