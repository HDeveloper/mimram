<!doctype html>

<html lang="en">

<head>
    <meta charset="utf-8">

    <title>IBC Vehicles - Mimram</title>

    <meta name="description" content="Mimram">
    <meta name="author" content="SitePoint">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick.css" />
    <link rel="stylesheet" href="dist/css/main.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/fd007505c4.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>



</head>

<body>
   <header class="header">
        <div class="header__wrapper">
            <div class="header__logo logo">
                <a href="index.php"><img src="assets/img/logo-trans.png" /></a>
            </div>
            <div class="header__nav nav">
                <ul>
                    

                    <li>
                        <a href="index.php#ourCompanies__section">our family of companies</a>
                        <ul>
                            <li>
                                <a href="ponspecial.php">PON Special Works</a>
                            </li>
                            <li>
                                <a href="mimramengineering.php">Mimram Engineering Service</a>
                            </li>
                            <li>
                                <a href="mimramspecial.php">Mimram Special Works</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="whatwedo.php">what we do</a>
                        <ul>
                            <li>
                                <a href="whatwedo.php#our_services">Our Services</a>
                            </li>
                            <li>
                                <a href="whatwedo.php#our_commitments">Our Commitments</a>
                            </li>
                            <li>
                                <a href="whatwedo.php#our_quality">Quality</a>
                            </li>
                            <li>
                                <a href="whatwedo.php#our_accreditations">Accreditations</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="casestudies.php">case studies</a>
                        
                    </li>
                    <li>
                        <a href="aboutus.php">about us</a>
                        <ul>
                            <li>
                                <a href="aboutus.php">About Us</a>
                            </li>
                            <li>
                                <a href="values.php">Our Values</a>
                            </li>
                            <li>
                                <a href="profiles.php">Profiles</a>
                            </li>
                            <li>
                                <a href="history.php">Our History</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="contact.php">contact</a></li>
                </ul>
            </div>
            <div class="header__contact contact">
                <ul>
                    <li>
                        <a href="mailto:info@mimramservices.com"><i class="fas fa-envelope"></i>
                            info@mimramservices.com</a>
                    </li>
                    <li>
                        <a href="tel:01582 568212"><i class="fas fa-phone-alt"></i> 01582 568212</a>
                    </li>
                </ul>
            </div>
            <div class="header__hamburger">
                <div id="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
    </header>

    </section>

    <section class="header__images">
        <div class="header__image"> <img src="assets/img/image--24--big.jpg"> </div>
    </section>
    <div class="caseStoryInfo">
        <div class="container">
            <div class="companies__header">
                <h1><span>IBC Vehicles, Luton</span></h1>
            </div>
            <div class="caseStoryInfo__text">
                <div class="caseStoryInfo__text__split">
                    <p>With a long association with IBC we have completed various projects over the years. Our services
                        provided includedmechanical and electrical services design and installation as well as some
                        turnkey developments where we offer a whole package solution to include fabric works. We have
                        also provided our services in plant relocations as a key partner on the overall logistics at IBC
                        in Luton we are called upon to assist in key projects that have both tight time frame and costs
                        constraints. Often these relocation projects are business critical as we are working in the live
                        production environment there is no room for delay or error.<br><br>
                        More recently we have been involved in energy efficiency directives and these include automated
                        control of lighting as well as the introduction of LED lighting technology on a like for like
                        replacement basis, some of these areas being highly specialised installation owing to their
                        environment, including Paintshop, K Block Body framing welding areas, Robot Cells and Elpo
                        (Electrocoat Paint Operation) areas. We have also carried out plant wide data infrastructure
                        installations providing Fibre Optic data networking as well as localised Cat6e infrastructure
                        cabling and connections.<br><br>
                        Mechanical works have involved many form of installations from the run of the mill HVCA services
                        to Production and Process works involving extremes of engineering with extremely high pressure
                        systems and high pressure steam installations to mammoth air distribution
                        systems.<br><br>Projects of recent note in this sector are:-<br><br><span class="bold navyBlue small__header">High
                            pressure wax line delivery system </span>
                        High pressure wax line delivery system operating at 360bar with our installations being tested
                        and certified to work at processes operating at 500bar, these pipe services adopted the use of
                        SAE flange assemblies and ermetti connections.<br><br>

                    </p>
                </div>
                <div class="caseStoryInfo__text__split">

                    <p>
                        The process includes high pressure sealant storage and delivery system to the underside of the
                        new Vauxhall & Peugeot commercial vehicle for sound deadening and underbody protection. This
                        system includes high pressure delivery pipework from track side pumping stations on a
                        recirculating system comprising of a centralised restocking tote. Track side delivery system
                        operating under varying pressure from 180 Bar to 360 Bar and compiled of both SAE Welded and
                        Ermeto systems. The installation of pipework that was via pre-assembled a Stauff Clamp system
                        which included rigorous tests to the system to ensure it operated at 500 Bar approximately 7200
                        PSI.<br><br>
                        <span class="bold navyBlue small__header">X82 HVAC Project <br></span>
                        This is no ordinary HVAC contract with two AHU’s providing m3 per, the main AHU weighed 19
                        metric tonnes and the provision of structural support stiffening being incorporated in our scope
                        of works. The project involved the installation of ventilation ducts with a diameter of 1000mm
                        and in order to co-ordinate these within the limited space provided within the structure
                        provided a unique challenge to which we provided a bespoke system of support brackets
                        effectively creating an external spine of air distribution on top of the IBC production facility
                        roof. As a design and build contractor we were tasked not only with the co-ordination of the
                        delivery systems and their duties but also the provision of high velocity extract systems to
                        welding apparatus in the form, of robot cells and manual welding stations. We developed bespoke
                        high velocity extraction plates in order to ensure that all welding fume was extinguished upon
                        entry to the ducted system. Interestingly and somewhat uncommon process work we also provided
                        useful heat recovery by way of incorporation of thermal wheels in each AHU.

                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="caseStudy__info">
        <div class="container">
            <div class="caseStudy__info__column">
               <div class="caseStudy__info__box">
                    <div class="caseStudy__info__box__icon">
                        <span><i class="fas fa-user"></i></span>
                    </div>
                    <div class="caseStudy__info__box__header">
                        <h2>Client</h2>
                    </div>
                    <div class="caseStudy__info__box__text">
                        <h5>IBC Vehicles</h5>
                    </div>
                </div>
                <div class="caseStudy__info__box">
                    <div class="caseStudy__info__box__icon">
                        <span><i class="fas fa-pound-sign"></i></span>
                    </div>
                    <div class="caseStudy__info__box__header">
                        <h2>Contract Value</h2>
                    </div>
                    <div class="caseStudy__info__box__text">
                        <h5>Approx. £100k PA</h5>
                    </div>
                </div>
                <div class="caseStudy__info__box">
                    <div class="caseStudy__info__box__icon">
                        <span><i class="far fa-clock"></i></span>
                    </div>
                    <div class="caseStudy__info__box__header">
                        <h2>Contract Duration</h2>
                    </div>
                    <div class="caseStudy__info__box__text">
                        <h5>30 years</h5>
                    </div>
                </div>
                <div class="caseStudy__info__box">
                    <div class="caseStudy__info__box__icon">
                        <span><i class="fas fa-map-marker-alt"></i></span>
                    </div>
                    <div class="caseStudy__info__box__header">
                        <h2>Location</h2>
                    </div>
                    <div class="caseStudy__info__box__text">
                        <h5>Luton, Bedfordshire</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer>
        <div class="footer__section1">
            <div class="footer__container">
                <div class="footer__section1__header">
                    <h5>Contact Us</h5>
                </div>
                <div class="footer__section1__links">
                    <ul class="footer">
                        <li class="footer"><a href="index.php#ourCompanies__section">Our Family of Companies</a></li>
                        <li class="footer"><a href="whatwedo.php">What We Do</a></li>
                        <li class="footer"><a href="casestudies.php">case studies</a></li>
                        <li class="footer"><a href="aboutus.php">About Us</a></li>
                        <li class="footer"><a href="contact.php">contact</a></li>
                    </ul>
                </div>
                <div class="footer__section1__contact">
                    <div class="footer__section1__contact__link1">
                        <div class="footer__section1__contact__link1__logo">
                            <span><i class="fas fa-envelope"></i></span>
                        </div>
                        <div class="footer__section1__contact__link1__text">
                            <a href="mailto:info@mimramservices.com">
                                <p>info@mimramservices.com</p>
                            </a>
                        </div>
                    </div>
                    <div class="footer__section1__contact__link2">
                        <div class="footer__section1__contact__link2__logo">
                            <span><i class="fas fa-phone-alt"></i></span>
                        </div>
                        <div class="footer__section1__contact__link2__text">
                            <a href="tel:01582 568212">
                                <p>01582 568212</p>
                            </a>
                        </div>
                    </div>
                    <div class="footer__section1__contact__link3">
                        <div class="footer__section1__contact__link3__logo">
                            <span><i class="fas fa-map-marker-alt"></i></span>
                        </div>
                        <div class="footer__section1__contact__link3__text">
                            <p>Unit 27 Sundon Industrial Estate, Dencora Way, Bedfordshire, LU3 3HP</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="footer__section2">
            <div class="footer__button">
                <a href="contact.php">
                    <button class="contact"><span>Contact Mimram</span></button>
                </a>
            </div>
            <div class="footer__section2__logo">
                <div class="footer__section2__logo__image">
                    <div class="footer__section2__logo__image__logo">
                        <img src="assets/img/logo.png">
                    </div>
                </div>
                <div class="footer__section2__logo__text">
                    <p>Efficient, effective engineering solutions</p>
                </div>
            </div>
            <div class="footer__section2__links">
                <div class="footer__section2__links">
                    <ul class="footer">
                        <li class="footer footerRight"><a class="footerRight">COPYRIGHT 2020</a></li>
                        <li class="footer"><a href="privacy.php" class="footerRight">PRIVACY</a></li>
                        <li class="footer"><a href="terms.php" class="footerRight">TERMS</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <script type="text/javascript" src="assets/js/slick/slick.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js"></script>

</body>

</html>
