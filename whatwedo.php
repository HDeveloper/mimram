<!doctype html>

<html lang="en">

<head>
    <meta charset="utf-8">

    <title>Mimram Engineering - Mimram</title>

    <meta name="description" content="Mimram">
    <meta name="author" content="SitePoint">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick.css" />
    <link rel="stylesheet" href="dist/css/main.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/fd007505c4.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>



</head>

<body>
    <header class="header">
        <div class="header__wrapper">
            <div class="header__logo logo">
                <a href="index.php"><img src="assets/img/logo-trans.png" /></a>
            </div>
            <div class="header__nav nav">
                <ul>


                    <li>
                        <a href="index.php#ourCompanies__section">our family of companies</a>
                        <ul>
                            <li>
                                <a href="ponspecial.php">PON Special Works</a>
                            </li>
                            <li>
                                <a href="mimramengineering.php">Mimram Engineering Service</a>
                            </li>
                            <li>
                                <a href="mimramspecial.php">Mimram Special Works</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="whatwedo.php">what we do</a>
                        <ul>
                            <li>
                                <a href="whatwedo.php#our_services">Our Services</a>
                            </li>
                            <li>
                                <a href="whatwedo.php#our_commitments">Our Commitments</a>
                            </li>
                            <li>
                                <a href="whatwedo.php#our_quality">Quality</a>
                            </li>
                            <li>
                                <a href="whatwedo.php#our_accreditations">Accreditations</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="casestudies.php">case studies</a>

                    </li>
                    <li>
                        <a href="aboutus.php">about us</a>
                        <ul>
                            <li>
                                <a href="aboutus.php">About Us</a>
                            </li>
                            <li>
                                <a href="values.php">Our Values</a>
                            </li>
                            <li>
                                <a href="profiles.php">Profiles</a>
                            </li>
                            <li>
                                <a href="history.php">Our History</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="contact.php">contact</a></li>
                </ul>
            </div>
            <div class="header__contact contact">
                <ul>
                    <li>
                        <a href="mailto:info@mimramservices.com"><i class="fas fa-envelope"></i>
                            info@mimramservices.com</a>
                    </li>
                    <li>
                        <a href="tel:01582 568212"><i class="fas fa-phone-alt"></i> 01582 568212</a>
                    </li>
                </ul>
            </div>
            <div class="header__hamburger">
                <div id="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
    </header>


    <section class="header__images">
        <div class="header__image"> <img src="assets/img/image--19--big.jpg"> </div>
    </section>
    <section class="our_services" id="our_services">
        <div class="container">
            <div class="our_services__header">
                <div class="companies__header">
                    <h1 class="navyBlue"><span>Our Services</span></h1>
                </div>
                <p>Our strength as a business lies within our wealth of experience and skill within our
                    teams of highly experienced staff who maintain independent and specialist
                    knowledge to the services we offer.</p>
            </div>
            <div class="our_services__flex">
                <div class="our_services__electrical">
                    <h2 class="navyBlue">Electrical</h2>
                    <p>Electrical installations are business-critical services. From emergency
                        lighting and power systems through to bespoke uninterruptable power distribution
                        systems. We have delivered landmark installations and we pride ourselves on not
                        just providing our service but working in collaboration with our clients to align
                        visions to ensure customer satisfaction with quality and engineering expertise.</p>
                    <ul class="circle noMargin">
                        <li class="circle"><a>AV Solutions</a></li>
                        <li class="circle"><a>Low Voltage</a></li>
                        <li class="circle"><a>Small Power Installation</a></li>
                        <li class="circle">
                            <a>Lighting (internal, External & Emergency)</a>
                        </li>
                        <li class="circle"><a>Multi-failsafe UPS </a></li>
                        <li class="circle"><a>CCTV </a></li>
                        <li class="circle">
                            <a>Intruder Alarm and Access Systems </a>
                        </li>
                        <li class="circle">
                            <a>Fire detection Systems, Fire Detection </a>
                        </li>
                        <li class="circle"><a>Home Automation</a></li>
                        <li class="circle"><a>Electrical Testing</a></li>
                        <li class="circle"><a>PAT Testing</a></li>
                    </ul>
                </div>
                <div class="our_services__mechanical">
                    <h2 class="navyBlue">Mechanical</h2>
                    <p>For residential, commercial or retail projects, our dedicated and multi-
                        disciplined team of plumbers, fitters, gas safe engineers and refcom engineers have
                        the in-depth knowledge and skill to provide a full range of plumbing, drainage and
                        heating solutions, for all sizes of mechanical installations. As an approved Contractor
                        with leading manufacturers our team have built the reputation on high quality and
                        standard. From the basics of gas, water and drainage, through to solutions that
                        comply with regulatory challenges, our team offer solutions that are price
                        competitive and cost effective.</p>
                    <ul class="circle noMargin">
                        <li class="circle"><a>Pipework</a></li>
                        <li class="circle"><a>Design install</a></li>
                        <li class="circle">
                            <a>Domestic and Commercial gas installation</a>
                        </li>
                        <li class="circle"><a>Above ground drainage</a></li>
                        <li class="circle"><a>LTHW Systems</a></li>
                        <li class="circle"><a>Central Plant</a></li>
                        <li class="circle"><a>Rainwater Services</a></li>
                        <li class="circle"><a>Fabrication works</a></li>
                        <li class="circle"><a>Ventilation Systems</a></li>
                        <li class="circle"><a>Air conditioning</a></li>
                        <li class="circle"><a>Underfloor Heating Systems</a></li>
                    </ul>
                </div>
            </div>
            <div class="our_services__design">
                <h2 class="navyBlue">Design &amp; Build</h2>
                <p>Our dynamic, in-house engineering team has the full range of
                    contemporary design technologies at its fingertips. We not only focus on simplifying project
                    management but help to keep the budget on track too, using craft solutions that are cost
                    effective and energy efficient.</p>
            </div>
        </div>
    </section>


    <section class="our__commitments" id="our_commitments">
        <div class="container">
            <div class="companies__header">
                <h1 class="navyBlue"><span>Our Commitments</span></h1>
            </div>
            <p>We recognise that the safety of all Staff, Contractors and anyone else affected by
                the Company’s day to day activities is of prime importance. That is why our Senior
                Management are fully committed to achieving a strong safety culture and believe
                that nothing is so important that we cannot take the time to do it safely.<br><br>
                We consider our health and safety obligations as a fundamental part of the regular
                running of our business and appreciate the demonstrable and personal commitment
                of everyone to not only establish but maintain such a culture where every
                individuals’ Health, Safety and Welfare requirements are taken into account.<br><br>
                We nurture our culture by developing the capabilities of all individuals within our
                Company through knowledge and practical experience to continually promote the
                better understanding and awareness of individuals own safety and others around
                them.<br><br>
                Through our quarterly internal Newsletters we hold Safety Campaigns to encourage
                all site staff, office staff and management to work together along with our clients,
                contractors and suppliers to uphold the duty of care to themselves, their team
                members and others associated and around them.<br><br>
                Our own in-house health &amp; safety Committee hold quarterly meetings to address
                issues and we have been nominated as safe contactor on recent projects. We are
                also members of Safecontractor as well as gold members for Constructionline.<br><br>
                For more information or a copy of our Health &amp; Safety Statement, please contact us.</p>
        </div>
    </section>

    <section class="our_quality" id="our_quality">
        <div class="container">
            <div class="our_services__flex">
                <div class="our_quality__text">
                    <div class="companies__header">
                        <h1 class="navyBlue"><span>Our Quality</span></h1>
                    </div>
                    <p>As a company we are all dedicated to providing a quality of service that not only
                        aligns with our core values but also a service that will keep customers returning time
                        after time. In doing so, we have adopted a system of Quality Management that
                        provides an assurance of quality that is fundamental to all work undertaken by the
                        Company.<br><br><span class="margin_bottom">This Quality Management System allows us to:</span></p>
                    <ul class="circle noMargin">
                        <li class="circle"><a>Consistently provide products and services that meet customer and
                                applicable statutory and regulatory requirements.</a></li>
                        <li class="circle"><a>Facilitate opportunities to enhance customer satisfaction.</a></li>
                        <li class="circle">
                            <a>Facilitate opportunities to enhance customer satisfaction.</a>
                        </li>
                        <li class="circle"><a>Demonstrate conformity to the specified Quality Management System
                                requirements.</a></li>
                    </ul>
                    <p class="marginTop"><span class="margin_bottom">We base our Quality Management System on the following principles:</span></p>
                    <ul class="circle noMargin">
                        <li class="circle"><a>Customer focus</a></li>
                        <li class="circle"><a>Leadership</a></li>
                        <li class="circle">
                            <a>Engagement of people</a>
                        </li>
                        <li class="circle"><a>Process approach</a></li>
                        <li class="circle"><a>Improvement</a></li>
                        <li class="circle"><a>Evidence-based decision making</a></li>
                        <li class="circle"><a>Relationship management</a></li>
                    </ul>
                </div>

                <div class="our_quality__image">
                    <img src="assets/img/image--22.jpg">
                </div>
            </div>
        </div>
    </section>

    <section class="accreditations" id="our_accreditations">


        <div class="accreditations_slider noPadding">
            <div class="container reducedContainer noPadding ">
                <div class="companies__header">
                    <h1 class="navyBlue"><span>Our Accreditations</span></h1>
                </div>


                <div class="arrow_next">
                    <img src="assets/img/arrow--right.png">
                </div>
                <div class="accreditation_row">
                    <div class="accreditation"><img src="assets/img/logo1.png"></div>
                    <div class="accreditation"><img src="assets/img/logo2.png"></div>
                    <div class="accreditation"><img src="assets/img/logo3.png"></div>
                    <div class="accreditation"><img src="assets/img/iso45001.png"></div>
                    <div class="accreditation"><img src="assets/img/contracter.jpg"></div>

                </div>
                <div class="arrow_prev">
                    <img src="assets/img/arrow--left.png">
                </div>
            </div>
        </div>
    </section>

    <section class="accreditations">
        <div class="container">
            <div class="accreditations__content">
                <div class="accreditations__image">
                    <img src="assets/img/accreditations.PNG" />
                </div>
                <div class="accreditations__text">
                    <div class="accreditations__info">
                        <p>
                            An accredited member of the Safe Contractor scheme, Mimram’s
                            health & safety policy is supported by stringent guidelines,
                            ongoing training and regular on-site inspections, a major
                            contribution to our excellent health and safety record. Our full
                            time, in-house health and safety managers work closely with
                            external Health and Safety consultants to ensure outstanding
                            safety practices.<br /><br />
                            Quality control is critical to successful M&E contracting, and
                            we maintain an in-house, full-time quality control department.
                            Our quality management system, which incorporates strategies for
                            continual improvement, has been QMS accredited to the global
                            benchmark standard ISO 9001.<br /><br />
                            Naturally, appropriate certifications ensure that our
                            technicians, electricians and mechanical engineers deliver
                            outstanding quality whilst remaining highly price-competitive.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="our_commitments">
        <div class="container">

        </div>
    </section>
    <section class="quality">
        <div class="container">

        </div>
    </section>
    <section class="our_accreditations">
        <div class="container">

        </div>
    </section>

    <footer>
        <div class="footer__section1">
            <div class="footer__container">
                <div class="footer__section1__header">
                    <h5>Contact Us</h5>
                </div>
                <div class="footer__section1__links">
                    <ul class="footer">
                        <li class="footer"><a href="index.php#ourCompanies__section">Our Family of Companies</a></li>
                        <li class="footer"><a href="whatwedo.php">What We Do</a></li>
                        <li class="footer"><a href="casestudies.php">case studies</a></li>
                        <li class="footer"><a href="aboutus.php">About Us</a></li>
                        <li class="footer"><a href="contact.php">contact</a></li>
                    </ul>
                </div>
                <div class="footer__section1__contact">
                    <div class="footer__section1__contact__link1">
                        <div class="footer__section1__contact__link1__logo">
                            <span><i class="fas fa-envelope"></i></span>
                        </div>
                        <div class="footer__section1__contact__link1__text">
                            <a href="mailto:info@mimramservices.com">
                                <p>info@mimramservices.com</p>
                            </a>
                        </div>
                    </div>
                    <div class="footer__section1__contact__link2">
                        <div class="footer__section1__contact__link2__logo">
                            <span><i class="fas fa-phone-alt"></i></span>
                        </div>
                        <div class="footer__section1__contact__link2__text">
                            <a href="tel:01582 568212">
                                <p>01582 568212</p>
                            </a>
                        </div>
                    </div>
                    <div class="footer__section1__contact__link3">
                        <div class="footer__section1__contact__link3__logo">
                            <span><i class="fas fa-map-marker-alt"></i></span>
                        </div>
                        <div class="footer__section1__contact__link3__text">
                            <p>Unit 27 Sundon Industrial Estate, Dencora Way, Bedfordshire, LU3 3HP</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="footer__section2">
            <div class="footer__button">
                <a href="contact.php">
                    <button class="contact"><span>Contact Mimram</span></button>
                </a>
            </div>
            <div class="footer__section2__logo">
                <div class="footer__section2__logo__image">
                    <div class="footer__section2__logo__image__logo">
                        <img src="assets/img/logo.png">
                    </div>
                </div>
                <div class="footer__section2__logo__text">
                    <p>Efficient, effective engineering solutions</p>
                </div>
            </div>
            <div class="footer__section2__links">
                <div class="footer__section2__links">
                    <ul class="footer">
                        <li class="footer footerRight"><a class="footerRight">COPYRIGHT 2020</a></li>
                        <li class="footer"><a href="privacy.php" class="footerRight">PRIVACY</a></li>
                        <li class="footer"><a href="terms.php" class="footerRight">TERMS</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <script type="text/javascript" src="assets/js/slick/slick.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js"></script>

</body>

</html>
