<!doctype html>

<html lang="en">

<head>
    <meta charset="utf-8">

    <title>Case Studies - Mimram</title>
    <meta name="description" content="Mimram">
    <meta name="author" content="SitePoint">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick.css" />
    <link rel="stylesheet" href="dist/css/main.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/fd007505c4.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>



</head>

<body>
    <header class="header">
        <div class="header__wrapper">
            <div class="header__logo logo">
                <a href="index.php"><img src="assets/img/logo-trans.png" /></a>
            </div>
            <div class="header__nav nav">
                <ul>
                    

                    <li>
                        <a href="index.php#ourCompanies__section">our family of companies</a>
                        <ul>
                            <li>
                                <a href="ponspecial.php">PON Special Works</a>
                            </li>
                            <li>
                                <a href="mimramengineering.php">Mimram Engineering Service</a>
                            </li>
                            <li>
                                <a href="mimramspecial.php">Mimram Special Works</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="whatwedo.php">what we do</a>
                        <ul>
                            <li>
                                <a href="whatwedo.php#our_services">Our Services</a>
                            </li>
                            <li>
                                <a href="whatwedo.php#our_commitments">Our Commitments</a>
                            </li>
                            <li>
                                <a href="whatwedo.php#our_quality">Quality</a>
                            </li>
                            <li>
                                <a href="whatwedo.php#our_accreditations">Accreditations</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="casestudies.php">case studies</a>
                        
                    </li>
                    <li>
                        <a href="aboutus.php">about us</a>
                        <ul>
                            <li>
                                <a href="aboutus.php">About Us</a>
                            </li>
                            <li>
                                <a href="values.php">Our Values</a>
                            </li>
                            <li>
                                <a href="profiles.php">Profiles</a>
                            </li>
                            <li>
                                <a href="history.php">Our History</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="contact.php">contact</a></li>
                </ul>
            </div>
            <div class="header__contact contact">
                <ul>
                    <li>
                        <a href="mailto:info@mimramservices.com"><i class="fas fa-envelope"></i>
                            info@mimramservices.com</a>
                    </li>
                    <li>
                        <a href="tel:01582 568212"><i class="fas fa-phone-alt"></i> 01582 568212</a>
                    </li>
                </ul>
            </div>
            <div class="header__hamburger">
                <div id="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
    </header>

    <section class="caseStudyTile">
        <div class="container">
            <div class="case__study__flex">
                <a class="case__study" href="higgins.php">
                    <div class="case__study__image">
                        <img src="assets/img/gabriel.jpg">
                    </div>
                    <div class="case__study__info">
                        <h2 class="navyBlue">Gabriel Square,<br> St Albans 
</h2>
                        <h3 class="orange">Residential</h3>
                    </div>
                </a>
                <a class="case__study" href="engie.php">

                    <div class="case__study__image">
                        <img src="assets/img/marsh2.jpg">
                    </div>
                    <div class="case__study__info">
                        <h2 class="navyBlue">Purley Center Development, <br>Marsh Farm </h2>
                        <h3 class="orange">Residential</h3>
                    </div>

                </a>
                <a class="case__study" href="lovegrove.php">

                    <div class="case__study__image">
                        <img src="assets/img/lovegrove2.jpg">
                    </div>
                    <div class="case__study__info">
                        <h2 class="navyBlue">No1 Lounges, <br>Luton </h2>
                        <h3 class="orange">Leisure</h3>
                    </div>

                </a>
                <a class="case__study" href="fattorie.php">

                    <div class="case__study__image">
                        <img src="assets/img/fattorie3.jpg">
                    </div>
                    <div class="case__study__info">
                        <h2 class="navyBlue">Fattorie Garofalo, <br>London Luton Airport 
</h2>
                        <h3 class="orange">Commercial</h3>
                    </div>

                </a>
                <a class="case__study" href="county.php">
                    <div class="case__study__image">
                        <img src="assets/img/image--16.jpg">
                    </div>
                    <div class="case__study__info">
                        <h2 class="navyBlue">Fairland’s Primary School, <br>Hertfordshire </h2>
                        <h3 class="orange">Education</h3>
                    </div>

                </a>
                <a class="case__study" href="morgan.php">

                    <div class="case__study__image">
                        <img src="assets/img/image--19--big.jpg">
                    </div>
                    <div class="case__study__info">
                        <h2 class="navyBlue">Oaklands College, <br>Welwyn Garden City </h2>
                        <h3 class="orange">Education</h3>
                    </div>

                </a>
                <a class="case__study" href="imtech.php">

                    <div class="case__study__image">
                        <img src="assets/img/image--25--big.jpg">
                    </div>
                    <div class="case__study__info">
                        <h2 class="navyBlue">John Radcliffe Hospital </h2>
                        <h3 class="orange">Healthcare</h3>
                    </div>

                </a>
                <a class="case__study" href="luton.php">

                    <div class="case__study__image">
                        <img src="assets/img/image--20--big.jpg">
                    </div>
                    <div class="case__study__info">
                        <h2 class="navyBlue">London Luton Airport, <br>Luton</h2>
                        <h3 class="orange">Travel</h3>
                    </div>

                </a>
                <a class="case__study" href="ibc.php">

                    <div class="case__study__image">
                        <img src="assets/img/image--24.jpg">
                    </div>
                    <div class="case__study__info">
                        <h2 class="navyBlue">IBC Vehicles, <br>Luton </h2>
                        <h3 class="orange">Industrial</h3>
                    </div>

                </a>

            </div>
        </div>
    </section>


    <footer>
        <div class="footer__section1">
            <div class="footer__container">
                <div class="footer__section1__header">
                    <h5>Contact Us</h5>
                </div>
                <div class="footer__section1__links">
                    <ul class="footer">
                        <li class="footer"><a href="index.php#ourCompanies__section">Our Family of Companies</a></li>
                        <li class="footer"><a href="whatwedo.php">What We Do</a></li>
                        <li class="footer"><a href="casestudies.php">case studies</a></li>
                        <li class="footer"><a href="aboutus.php">About Us</a></li>
                        <li class="footer"><a href="contact.php">contact</a></li>
                    </ul>
                </div>
                <div class="footer__section1__contact">
                    <div class="footer__section1__contact__link1">
                        <div class="footer__section1__contact__link1__logo">
                            <span><i class="fas fa-envelope"></i></span>
                        </div>
                        <div class="footer__section1__contact__link1__text">
                            <a href="mailto:info@mimramservices.com">
                                <p>info@mimramservices.com</p>
                            </a>
                        </div>
                    </div>
                    <div class="footer__section1__contact__link2">
                        <div class="footer__section1__contact__link2__logo">
                            <span><i class="fas fa-phone-alt"></i></span>
                        </div>
                        <div class="footer__section1__contact__link2__text">
                            <a href="tel:01582 568212">
                                <p>01582 568212</p>
                            </a>
                        </div>
                    </div>
                    <div class="footer__section1__contact__link3">
                        <div class="footer__section1__contact__link3__logo">
                            <span><i class="fas fa-map-marker-alt"></i></span>
                        </div>
                        <div class="footer__section1__contact__link3__text">
                            <p>Unit 27 Sundon Industrial Estate, Dencora Way, Bedfordshire, LU3 3HP</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="footer__section2">
            <div class="footer__button">
                <a href="contact.php">
                    <button class="contact"><span>Contact Mimram</span></button>
                </a>
            </div>
            <div class="footer__section2__logo">
                <div class="footer__section2__logo__image">
                    <div class="footer__section2__logo__image__logo">
                        <img src="assets/img/logo.png">
                    </div>
                </div>
                <div class="footer__section2__logo__text">
                    <p>Efficient, effective engineering solutions</p>
                </div>
            </div>
            <div class="footer__section2__links">
                <div class="footer__section2__links">
                    <ul class="footer">
                        <li class="footer footerRight"><a class="footerRight">COPYRIGHT 2020</a></li>
                        <li class="footer"><a href="privacy.php" class="footerRight">PRIVACY</a></li>
                        <li class="footer"><a href="terms.php" class="footerRight">TERMS</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <script type="text/javascript" src="assets/js/slick/slick.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js"></script>

</body>

</html>
